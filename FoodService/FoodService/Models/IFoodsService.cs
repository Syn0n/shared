﻿using System;
using System.Collections.Generic;

namespace FoodService.Models
{
    public interface IFoodsService
    {
        IEnumerable<Category> Categories { get; }
        IEnumerable<Food> Foods { get; }
        IEnumerable<Customer> Customers { get; }
        IEnumerable<Basket> Baskets { get; }
        IEnumerable<FoodBasket> FoodBaskets { get; }
        IEnumerable<Food> GetFoodsByCategoryId(Int32? categoryId);
        List<Food> GetTop10Food();
        Food GetFood(Int32? foodId);
        List<Food> GetNonOrderedFoodsByBasketId(Int32 basketId);
        List<Food> GetFoodsByBasketId(Int32 basketId);
        Basket GetBasket(Int32? basketId);
        Basket GetNonOrderedBasket(Int32? basketId);
        Basket GetBasketByCustomerId(Int32? customerId);
        void AddBasket(Basket basket);
        void UpdateBasket(Basket basket);
        void AddFoodToFoodBasket(Int32 basketId, Int32 foodId);
        void AddFoodToOrderedFoodBasket(Int32 customerbasketId, Int32 basketId, Int32 foodId);
        void RemoveFoodFromFoodBasket(Int32 basketId, Int32 foodId);
        IEnumerable<Int32> GetFoodImageIds(Int32? foodId);
        Byte[] GetFoodMainImage(Int32? foodId);
        Byte[] GetFoodImage(Int32? imageId, Boolean large);
        Customer GetCustomer(String userName);
        void UpdateCustomer(Customer customer);
        OrderViewModel NewOrder(Int32? basketId);
        Boolean SaveOrder(OrderViewModel order);
        Int32 GetPrice(OrderViewModel order);
    }
}
