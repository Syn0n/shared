﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;

namespace FoodService.Models
{
    public class AccountService : IAccountService
    {
        private readonly FoodServiceContext _context;

        public AccountService(FoodServiceContext context)
        {
            _context = context;
        }

        public Customer GetCustomer(String userName)
        {
            if (userName == null)
                return null;

            return _context.Customers.FirstOrDefault(c => c.UserName == userName);
        }

        public Boolean Login(LoginViewModel user)
        {
            if (user == null)
                return false;

            if (!Validator.TryValidateObject(user, new ValidationContext(user, null, null), null))
                return false;

            Customer customer = _context.Customers.FirstOrDefault(c => c.UserName == user.UserName);

            if (customer == null)
                return false;

            Byte[] passwordBytes = null;
            using (SHA512CryptoServiceProvider provider = new SHA512CryptoServiceProvider())
            {
                passwordBytes = provider.ComputeHash(Encoding.UTF8.GetBytes(user.UserPassword));
            }

            if (!passwordBytes.SequenceEqual(customer.UserPassword))
                return false;

            Basket basket = _context.Baskets.FirstOrDefault(b => b.CustomerId == customer.Id);

            if (basket == null)
            {
                basket = new Basket()
                {
                    TotalPrice = 0,
                    OrderedFoods = new List<Food>(),
                    Done = false,
                    Ordered = false,
                    CustomerId = customer.Id
                };
                _context.Baskets.Add(basket);
                _context.SaveChanges();
            }

            return true;
        }

        public Boolean Logout()
        {
            return true;
        }

        public Boolean Register(RegistrationViewModel customer)
        {
            if (customer == null)
                return false;

            if (!Validator.TryValidateObject(customer, new ValidationContext(customer, null, null), null))
                return false;

            if (_context.Customers.Count(c => c.UserName == customer.UserName) != 0)
                return false;

            Byte[] passwordBytes = null;
            using (SHA512CryptoServiceProvider provider = new SHA512CryptoServiceProvider())
            {
                passwordBytes = provider.ComputeHash(Encoding.UTF8.GetBytes(customer.UserPassword));
            }

            _context.Customers.Add(new Customer
            {
                Name = customer.CustomerName,
                Address = customer.CustomerAddress,
                Email = customer.CustomerEmail,
                PhoneNumber = customer.CustomerPhoneNumber,
                UserName = customer.UserName,
                UserPassword = passwordBytes
            });

            try
            {
                _context.SaveChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public Boolean Create(CustomerViewModel customer, out String userName)
        {
            userName = "user" + Guid.NewGuid();

            if (customer == null)
                return false;

            if (!Validator.TryValidateObject(customer, new ValidationContext(customer, null, null), null))
                return false;

            _context.Customers.Add(new Customer
            {
                Name = customer.CustomerName,
                Address = customer.CustomerAddress,
                Email = customer.CustomerEmail,
                PhoneNumber = customer.CustomerPhoneNumber,
                UserName = userName
            });

            try
            {
                _context.SaveChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}