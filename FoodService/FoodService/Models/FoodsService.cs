﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace FoodService.Models
{
    
    public class FoodsService : IFoodsService
    {
        private readonly FoodServiceContext _context;
        public FoodsService(FoodServiceContext context)
        {
            _context = context;
        }

        public IEnumerable<Food> Foods => _context.Foods.Include(b => b.Category);
        public IEnumerable<Category> Categories => _context.Categories;
        public IEnumerable<Customer> Customers => _context.Customers;
        public IEnumerable<Basket> Baskets => _context.Baskets;
        public IEnumerable<FoodBasket> FoodBaskets => _context.FoodBaskets;

        public Food GetFood(Int32? foodId)
        {
            if (foodId== null)
                return null;

            return _context.Foods
                .Include(b => b.Category)
                .FirstOrDefault(food => food.Id == foodId);
        }

        public List<Food> GetNonOrderedFoodsByBasketId(Int32 basketId)
        {
            var foodbaskets = _context.FoodBaskets
                .Include(o => o.Food)
                .Where(foodbasket => foodbasket.BasketId == basketId && foodbasket.Ordered == false).ToList();

            List<Food> foods = new List<Food>();
            foreach (FoodBasket fb in foodbaskets)
            {
                foods.Add(GetFood(fb.FoodId));
            }

            return foods;
        }

        public List<Food> GetFoodsByBasketId(Int32 basketId)
        {
            var foodbaskets = _context.FoodBaskets
                .Include(o => o.Food)
                .Where(foodbasket => foodbasket.BasketId == basketId).ToList();

            List<Food> foods = new List<Food>();
            foreach (FoodBasket fb in foodbaskets)
            {
                foods.Add(GetFood(fb.FoodId));
            }

            return foods;
        }

        public IEnumerable<Food> GetFoodsByCategoryId(Int32? categoryId)
        {
            if (categoryId == null || !_context.Foods.Any(food => food.CategoryId == categoryId))
                return null;

            return _context.Foods.Where(food => food.CategoryId == categoryId);
        }

        public List<Food> GetTop10Food()
        {
            FoodBasket[] ordered = _context.FoodBaskets.Where(f => f.Ordered == true).ToArray();
            Int32 orderedNumber = ordered.Count();
            Int32 foodNumber = _context.Foods.Count();
            Tuple<Int32, Int32>[] QuantityOfOrderedFood = new Tuple<Int32, Int32>[foodNumber];

            for (int i = 1; i <= foodNumber; i++)
            {
                Int32 ctr = 0;
                for (int j = 0; j < orderedNumber; j++)
                {
                    if (ordered[j].FoodId == i)
                        ctr++;
                }
                QuantityOfOrderedFood[i - 1] = new Tuple<Int32, Int32>(i,ctr);
            }

            Tuple<Int32, Int32>[] SortedQuantity = (from tuple in QuantityOfOrderedFood
                                                    orderby tuple.Item2 descending
                                                    select tuple).ToArray();

            List<Food> top10Food = new List<Food>();
            for (int i = 0; i < 10; i++)
            {
                top10Food.Add(GetFood(SortedQuantity[i].Item1));
            }

            return top10Food;
        }

        public Basket GetBasket(Int32? basketId)
        {
            if (basketId == null)
                return null;

            return _context.Baskets
                .Where(b => b.Id == basketId)
                .FirstOrDefault();
        }

        public Basket GetNonOrderedBasket(Int32? basketId)
        {
            if (basketId == null)
                return null;

            return _context.Baskets
                .Where(b => b.Id == basketId && b.Ordered == false)
                .FirstOrDefault();
        }

        public Basket GetBasketByCustomerId(Int32? customerId)
        {
            if (customerId == null)
                return null;

            return _context.Baskets
                .Where(b => b.CustomerId == customerId && b.Ordered == false)
                .FirstOrDefault();
        }

        public void AddBasket(Basket basket)
        {
            _context.Baskets.Add(basket);
            _context.SaveChanges();
        }

        public void UpdateBasket(Basket basket)
        {
            _context.Baskets.Update(basket);
            _context.SaveChanges();
        }

        public void AddFoodToFoodBasket(Int32 basketId, Int32 foodId)
        {
            FoodBasket foodbasket = new FoodBasket()
            {
                BasketId = basketId,
                FoodId = foodId,
                Ordered = false
            };
            _context.FoodBaskets.Add(foodbasket);
            _context.SaveChanges();
        }

        public void AddFoodToOrderedFoodBasket(Int32 customerbasketId, Int32 basketId, Int32 foodId)
        {
            FoodBasket foodbasket = _context.FoodBaskets.
                FirstOrDefault(f => f.BasketId == customerbasketId && f.FoodId == foodId && f.Ordered == false);

            foodbasket.Ordered = true;
            foodbasket.BasketId = basketId;

            _context.FoodBaskets.Update(foodbasket);
            _context.SaveChanges();
        }

        public void RemoveFoodFromFoodBasket(Int32 basketId, Int32 foodId)
        {
             FoodBasket foodbasket = _context.FoodBaskets.
                FirstOrDefault(a => a.BasketId == basketId &&
                               a.FoodId == foodId && a.Ordered == false);

             _context.FoodBaskets.Remove(foodbasket);
            _context.SaveChanges();
        }

        public IEnumerable<Int32> GetFoodImageIds(Int32? foodId)
        {
            if (foodId == null)
                return null;

            return _context.FoodImages
                .Where(image => image.FoodId == foodId)
                .Select(image => image.Id);
        }

        public Byte[] GetFoodMainImage(Int32? foodId)
        {
            if (foodId == null)
                return null;

            return _context.FoodImages
                .Where(image => image.FoodId == foodId)
                .Select(image => image.ImageSmall)
                .FirstOrDefault();
        }

        public Byte[] GetFoodImage(Int32? imageId, Boolean large)
        {
            if (imageId == null)
                return null;

            Byte[] imageContent = _context.FoodImages
                .Where(image => image.Id == imageId)
                .Select(image => large ? image.ImageLarge : image.ImageSmall)
                .FirstOrDefault();

            return imageContent;
        }

        public void UpdateCustomer(Customer customer)
        {
            Customer updatedCustomer = _context.Customers.FirstOrDefault(c => c.Id == customer.Id);
            updatedCustomer.Name = customer.Name;
            updatedCustomer.UserName = customer.UserName;
            updatedCustomer.Address = customer.Address;
            updatedCustomer.PhoneNumber = customer.PhoneNumber;
            updatedCustomer.Email = customer.Email;
            updatedCustomer.UserPassword = customer.UserPassword;

            _context.Customers.Update(updatedCustomer);
            _context.SaveChanges();
        }

        public Customer GetCustomer(String userName)
        {
            if (userName == null)
                return null;

            return _context.Customers.FirstOrDefault(c => c.UserName == userName);
        }

        public OrderViewModel NewOrder(Int32? basketId)
        {
            OrderViewModel order = new OrderViewModel();

            order.Basket = GetBasket(basketId);
            order.Basket.OrderedFoods = GetNonOrderedFoodsByBasketId(order.Basket.Id);

            return order;
        }

        public Boolean SaveOrder(OrderViewModel order)
        {
            if (order == null)
                return false;

            if (!Validator.TryValidateObject(order, new ValidationContext(order, null, null), null))
                return false;

            Basket basket = new Basket
            {
                Name = order.CustomerName,
                Email = order.CustomerEmail,
                BillingAddress = order.CustomerAddress,
                PhoneNumber = order.CustomerPhoneNumber,
                Ordered = true,
                Done = false,
                TotalPrice = order.Basket.TotalPrice,
                CustomerId = order.Basket.CustomerId,
                OrderedDate = DateTime.Now
            };
            List<Food> foods = GetNonOrderedFoodsByBasketId(order.Basket.Id);

            _context.Baskets.Add(basket);

            Basket customerbasket = _context.Baskets.FirstOrDefault(b => b.Id == order.Basket.Id);
            customerbasket.TotalPrice = 0;
            _context.Baskets.Update(customerbasket);

            foreach (Food food in foods)
            {
                AddFoodToOrderedFoodBasket(customerbasket.Id, basket.Id, food.Id);
            }

            try
            {
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public Int32 GetPrice(OrderViewModel order)
        {
            if (order == null)
                return 0;

            return order.Basket.TotalPrice;
        }
    }
}
