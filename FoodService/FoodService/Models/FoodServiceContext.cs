﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FoodService.Models
{
	public class FoodServiceContext : IdentityDbContext<Colleague, IdentityRole<int>, int>
    {
		public FoodServiceContext (DbContextOptions<FoodServiceContext> options) : base(options)  { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Colleague>().ToTable("Colleagues");
        }
        public DbSet<Food> Foods { get; set; }
        public DbSet<FoodBasket> FoodBaskets { get; set; }
        public DbSet<FoodImage> FoodImages { get; set; }
        public DbSet<Basket> Baskets { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}