﻿using System;

namespace FoodService.Models
{
    public interface IAccountService
    {
        Customer GetCustomer(String userName);

        Boolean Login(LoginViewModel user);

        Boolean Logout();

        Boolean Register(RegistrationViewModel customer);

        Boolean Create(CustomerViewModel guest, out String userName);
    }
}
