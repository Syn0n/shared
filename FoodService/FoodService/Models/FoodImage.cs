﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodService.Models
{
    public class FoodImage
    {
        public int Id { get; set; }
        public int FoodId { get; set; }
        public byte[] ImageSmall { get; set; }
        public byte[] ImageLarge { get; set; }
        public Food Food { get; set; }
    }
}
