﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace FoodService.Models
{
    public class OrderViewModel : CustomerViewModel
    {
        public Basket Basket { get; set; }
    }
}
