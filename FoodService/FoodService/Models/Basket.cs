﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FoodService.Models
{
    public class Basket
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String BillingAddress { get; set; }
        public String Email { get; set; }
        public String PhoneNumber { get; set; }
        public Int32 CustomerId { get; set; }
        public List<Food> OrderedFoods { get; set; }
        public Boolean Ordered { get; set; }
        public Boolean Done { get; set; }
        public Int32 TotalPrice { get; set; }
        public DateTime OrderedDate { get; set; }
        public DateTime DoneDate { get; set; }
    }
}