﻿using System;
using System.Collections.Generic;

namespace FoodService.Models
{
    public class Category
    {
        public Category()
        {
            Foods = new HashSet<Food>();
        }

        public String Name { get; set; }
        public Int32 Id { get; set; }
        public ICollection<Food> Foods { get; set; }
    }
}