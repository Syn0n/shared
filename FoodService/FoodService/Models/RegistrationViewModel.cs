﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FoodService.Models
{
    public class RegistrationViewModel : CustomerViewModel
    {
        [Required(ErrorMessage = "Username.")]
        [RegularExpression("^[A-Za-z0-9_-]{5,40}$", ErrorMessage = "Username is not in the right form.")]
        public String UserName { get; set; }

        [Required(ErrorMessage = "Password required.")]
        [RegularExpression("^[A-Za-z0-9_-]{5,40}$", ErrorMessage = "Password is not in the right form or in length(5-40).")]
        [DataType(DataType.Password)]
        public String UserPassword { get; set; }

        [Required(ErrorMessage = "Password again.")]
        [Compare(nameof(UserPassword), ErrorMessage = "Not the same password.")]
        [DataType(DataType.Password)]
        public String UserConfirmPassword { get; set; }
    }
}