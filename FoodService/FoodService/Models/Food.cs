﻿using System;

namespace FoodService.Models
{
    public class Food
    {
        public String Name { get; set; }
        public Category Category { get; set; }
        public Int32 CategoryId { get; set; }
        public String Description { get; set; }
        public Int32 Price { get; set; }
        public Boolean Spicy { get; set; }
        public Boolean Vegetarian { get; set; }
        public Int32 Id { get; set; }
    }
}