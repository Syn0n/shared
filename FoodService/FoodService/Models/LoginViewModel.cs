﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FoodService.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Username required.")]
        public String UserName { get; set; }

        [Required(ErrorMessage = "Password required.")]
        [DataType(DataType.Password)]
        public String UserPassword { get; set; }
    }
}