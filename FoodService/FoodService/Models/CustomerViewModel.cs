﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FoodService.Models
{
    public class CustomerViewModel
    {
        [Required(ErrorMessage = "Name required.")]
        [StringLength(60, ErrorMessage = "Max 60 chars.")]
        public String CustomerName { get; set; }


        [Required(ErrorMessage = "E-mail required.")]
        [EmailAddress(ErrorMessage = "E-mail is not in the right form.")]
        [DataType(DataType.EmailAddress)]
        public String CustomerEmail { get; set; }

        [Required(ErrorMessage = "Address required.")]
        public String CustomerAddress { get; set; }

        [Required(ErrorMessage = "Phone number required.")]
        [Phone(ErrorMessage = "Phone number is not in the right form.")]
        [DataType(DataType.PhoneNumber)]
        public String CustomerPhoneNumber { get; set; }
    }
}