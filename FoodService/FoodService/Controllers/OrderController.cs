﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using FoodService.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Http;

namespace FoodService.Controllers
{
    public class OrderController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly IFoodsService _foodService;

        public OrderController(IAccountService accountService, IFoodsService foodService)
        {
            _accountService = accountService;
            _foodService = foodService;
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);

            ViewBag.Categories = _foodService.Categories.ToArray();
        }

        [HttpGet]
        public IActionResult Index(Int32 basketId)
        {
            OrderViewModel order = _foodService.NewOrder(basketId);

            if (order == null || order.Basket.OrderedFoods.Count() == 0)
                return RedirectToAction("Index", "Home");

            if (HttpContext.Session.GetString("user") != null)
            {
                Customer customer = _accountService.GetCustomer(HttpContext.Session.GetString("user"));

                if (customer != null)
                {
                    order.CustomerAddress = customer.Address;
                    order.CustomerEmail = customer.Email;
                    order.CustomerName = customer.Name;
                    order.CustomerPhoneNumber = customer.PhoneNumber;
                }
            }

            return View("Index", order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(Int32? basketId, OrderViewModel order)
        {
            if (basketId == null || order == null)
                return RedirectToAction("Index", "Home");

            order.Basket = _foodService.GetNonOrderedBasket(basketId);

            if (order.Basket == null)
                return RedirectToAction("Index", "Home");

            if (!ModelState.IsValid)
                return View("Index", order);

            String userName;
            if (HttpContext.Session.GetString("user") != null)
            {
                userName = HttpContext.Session.GetString("user");
            }
            else
            {
                ModelState.AddModelError("", "Order was not successful, try it again!");
                return View("Index", order);
            }

            if (!_foodService.SaveOrder(order))
            {
                ModelState.AddModelError("", "Order was not successful, try it again!");
                return View("Index", order);
            }

            ViewBag.Message = "Order was successfully created!";
            return View("Result", order);
        }

        public FileResult BasketImage()
        {
            return File("~/images/basketImage.png", "image/png");
        }
    }
}
