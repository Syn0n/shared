﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using FoodService.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Filters;

namespace FoodService.Controllers
{
    public class HomeController : Controller
    {
        private readonly IFoodsService _foodService;
        public HomeController(IFoodsService foodService)
        {
            _foodService = foodService;
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);

            ViewBag.Categories = _foodService.Categories.ToArray();
        }

        public IActionResult Index()
        {
            ViewBag.WhichSearch = 0;

            var foods = _foodService.GetTop10Food();

            return View("Index", _foodService.GetTop10Food());
        }

        public IActionResult List(Int32 categoryId)
        {
            List<Food> foods = _foodService.GetFoodsByCategoryId(categoryId).ToList();

            if (foods.Count == 0)
                return RedirectToAction(nameof(Index));

            ViewBag.WhichSearch = categoryId;

            return View("Index", foods);
        }

        public IActionResult Details(Int32? foodId)
        {
            Food food = _foodService.GetFood(foodId);

            if (food == null)
                return RedirectToAction(nameof(Index));

            ViewBag.Title = "Food details: " + food.Name + " (" + food.Category.Name + ")";
            ViewBag.Images = _foodService.GetFoodImageIds(food.Id).ToList();

            return View("Details", food);
        }

        public IActionResult FoodSearching(string searchString, Int32 categoryId)
        {
            IEnumerable<Food> foods = from f in _foodService.Foods select f;
            if (!String.IsNullOrEmpty(searchString))
            {
                switch (categoryId)
                {
                    case 0: foods = foods.Where(s => s.Name.Contains(searchString)); break;
                    default: foods = foods.Where(s => s.Name.Contains(searchString) && s.CategoryId == categoryId); break;
                }
            }
            ViewBag.WhichSearch = categoryId;

            return View("Index", foods.ToList());
        }

        public FileResult ImageForFood(Int32? foodId)
        {
            Byte[] imageContent = _foodService.GetFoodMainImage(foodId);

            if (imageContent == null)
                return File("~/images/NoImage.png", "image/png");

            return File(imageContent, "image/png");
        }

        public FileResult Image(Int32? imageId, Boolean large = false)
        {
            Byte[] imageContent = _foodService.GetFoodImage(imageId, large);

            if (imageContent == null)
                return File("~/images/NoImage.png", "image/png");

            return File(imageContent, "image/png");
        }

        public FileResult BasketImage()
        {
            return File("~/images/basketImage.png", "image/png");
        }
    }
}
