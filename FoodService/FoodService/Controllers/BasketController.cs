﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FoodService.Models;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Http;

namespace FoodService.Controllers
{
    public class BasketController : Controller
    {
        private readonly IFoodsService _foodService;

        public BasketController(IFoodsService foodService)
        {
            _foodService = foodService;
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);

            ViewBag.Categories = _foodService.Categories.ToArray();
        }

        [HttpGet]
        public IActionResult Index()
        {
            var customer = _foodService.GetCustomer(HttpContext.Session.GetString("user"));

            if (customer == null)
                return RedirectToAction("Index", "Home");

            var basket = _foodService.GetBasketByCustomerId(customer.Id);

            if (basket == null)
                return RedirectToAction("Index", "Home");

            basket.OrderedFoods = _foodService.GetNonOrderedFoodsByBasketId(basket.Id);

            return View(basket);
        }

        public ActionResult Add(Int32 foodId)
        {
            var customer = _foodService.GetCustomer(HttpContext.Session.GetString("user"));
            if (customer == null)
                return RedirectToAction("Index", "Home");

            var basket = _foodService.GetBasketByCustomerId(customer.Id);
            if (basket == null)
                return RedirectToAction("Index", "Home");

            Food food = _foodService.GetFood(foodId);
            if (basket.TotalPrice + food.Price <= 20000)
            {
                _foodService.AddFoodToFoodBasket(basket.Id, food.Id);
                basket.TotalPrice += food.Price;
            }
            _foodService.UpdateBasket(basket);

            return RedirectToAction("Index");
        }

        public ActionResult Remove(Int32 foodId)
        {
            var customer = _foodService.GetCustomer(HttpContext.Session.GetString("user"));
            if (customer == null)
                return RedirectToAction("Index", "Home");

            var basket = _foodService.GetBasketByCustomerId(customer.Id);
            if (basket == null)
                return RedirectToAction("Index", "Home");


            Food food = _foodService.Foods.FirstOrDefault(f => f.Id == foodId);
            basket.TotalPrice -= food.Price;

            _foodService.UpdateBasket(basket);
            _foodService.RemoveFoodFromFoodBasket(basket.Id, food.Id);

            basket.OrderedFoods = _foodService.GetNonOrderedFoodsByBasketId(basket.Id);

            return View("Index", basket);
        }

        public ActionResult EmptyBasket()
        {
            var customer = _foodService.GetCustomer(HttpContext.Session.GetString("user"));
            if (customer == null)
                return RedirectToAction("Index", "Home");

            var basket = _foodService.GetBasketByCustomerId(customer.Id);
            if (basket == null)
                return RedirectToAction("Index", "Home");

            basket.TotalPrice = 0;
            _foodService.UpdateBasket(basket);

            List<Food> foods = _foodService.GetNonOrderedFoodsByBasketId(basket.Id);

            foreach (Food food in foods)
            {
                _foodService.RemoveFoodFromFoodBasket(basket.Id, food.Id);
            }

            return RedirectToAction("Index");
        }

        public FileResult ImageForFood(Int32? foodId)
        {
            Byte[] imageContent = _foodService.GetFoodMainImage(foodId);

            if (imageContent == null)
                return File("~/images/NoImage.png", "image/png");

            return File(imageContent, "image/png");
        }

        public FileResult BasketImage()
        {
            return File("~/images/basketImage.png", "image/png");
        }
    }
}