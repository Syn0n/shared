﻿using FoodService.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;

namespace FoodService.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly IFoodsService _foodService;

        public AccountController(IAccountService accountService, IFoodsService foodService)
        {
            _accountService = accountService;
            _foodService = foodService;
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);

            ViewBag.Categories = _foodService.Categories.ToArray();
        }

        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View("Login");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel user)
        {
            if (!ModelState.IsValid)
                return View("Login", user);

            if (!_accountService.Login(user))
            {
                ModelState.AddModelError("", "Bad username, or password.");
                return View("Login", user);
            }

            HttpContext.Session.SetString("user", user.UserName);                 
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View("Register");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegistrationViewModel customer)
        {
            if (!ModelState.IsValid)
                return View("Register", customer);

            if (!_accountService.Register(customer))
            {
                ModelState.AddModelError("UserName", "The given UserName already exists.");
                return View("Register", customer);
            }

            ViewBag.Information = "Registration was successful. Please log in.";

            if (HttpContext.Session.GetString("user") != null)
                HttpContext.Session.Remove("user");

            return RedirectToAction("Login");
        }

        public ActionResult Logout()
        {
            if (HttpContext.Session.GetString("user") != null)
                HttpContext.Session.Remove("user");

            return RedirectToAction("Index", "Home");
        }

        public FileResult BasketImage()
        {
            return File("~/images/basketImage.png", "image/png");
        }
    }
}