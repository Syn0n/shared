﻿using System;
using System.Collections.Generic;

namespace FoodService.Data
{
    public class FoodDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public CategoryDTO Category { get; set; }
        public Int32 CategoryId { get; set; }
        public Int32 Price { get; set; }
        public Boolean Spicy  { get; set; }
        public Boolean Vegetarian  { get; set; }
        public String Description  { get; set; }
        public IList<ImageDTO> Images { get; set; }
        public override Boolean Equals(Object obj)
        {
            return (obj is FoodDTO dto) && Name == dto.Name;
        }
    }
}
