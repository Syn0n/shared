﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodService.Data
{
    public class ImageDTO
    {
        public Int32 Id { get; set; }
        public Int32 FoodId { get; set; }
        public Byte[] ImageSmall { get; set; }
        public Byte[] ImageLarge { get; set; }
    }
}
