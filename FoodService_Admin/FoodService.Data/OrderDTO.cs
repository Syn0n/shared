﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodService.Data
{
    public class OrderDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String BillingAddress { get; set; }
        public String Email { get; set; }
        public String PhoneNumber { get; set; }
        public Int32 CustomerId { get; set; }
        public IList<FoodDTO> OrderedFoods { get; set; }
        public Boolean Ordered { get; set; }
        public Boolean Done { get; set; }
        public Int32 TotalPrice { get; set; }
        public DateTime OrderedDate { get; set; }
        public DateTime DoneDate { get; set; }
    }
}
