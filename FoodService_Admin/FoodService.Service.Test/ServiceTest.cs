﻿using System;
using System.Collections.Generic;
using System.Linq;
using FoodService.Data;
using FoodService.Service.Controllers;
using FoodService.Service.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace FoodService.Service.Test
{
    public class ServiceTest : IDisposable
    {
        private readonly FoodServiceContext _context;
        private readonly List<CategoryDTO> _categoryDTOs;
        private readonly List<FoodDTO> _foodDTOs;

        public ServiceTest()
        {
            var options = new DbContextOptionsBuilder<FoodServiceContext>()
                .UseInMemoryDatabase("ServiceTest")
                .Options;

            _context = new FoodServiceContext(options);
            _context.Database.EnsureCreated();

            var categoryData = new List<Category>
            {
                new Category { Name = "TESTCATEGORY" }
            };
            _context.Categories.AddRange(categoryData);

            var foodData = new List<Food>
            {
                new Food { Category = categoryData[0], Name = "TESTFOOD1", Spicy = true, Vegetarian = false},
                new Food { Category = categoryData[0], Name = "TESTFOOD2", Spicy = false, Vegetarian = true },
                new Food { Category = categoryData[0], Name = "TESTFOOD3", Spicy = true, Vegetarian = false }
            };
            _context.Foods.AddRange(foodData);
            _context.SaveChanges();

            _categoryDTOs = categoryData.Select(category => new CategoryDTO
            {
                Id = category.Id,
                Name = category.Name
            }).ToList();

            _foodDTOs = foodData.Select(food => new FoodDTO
            {
                Id = food.Id,
                Name = food.Name,
                Category = _categoryDTOs.Single(cat => cat.Id == food.CategoryId),
                Description = food.Description,
                Images = new List<ImageDTO>(),
                Spicy = food.Spicy,
                Vegetarian = food.Vegetarian,
                Price = food.Price
            }).ToList();
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [Fact]
        public void GetCategoriesTest()
        {
            var controller = new CategoriesController(_context);
            var result = controller.GetCategories();

            // Assert
            var objectResult = Assert.IsType<OkObjectResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<CategoryDTO>>(objectResult.Value);
            Assert.Equal(_categoryDTOs, model);
        }
        
        [Fact]
        public void GetFoodTest()
        {
            var controller = new FoodsController(_context);
            var result = controller.GetFoods();

            // Assert
            var objectResult = Assert.IsType<OkObjectResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<FoodDTO>>(objectResult.Value);
            Assert.Equal(_foodDTOs, model);
        }
        
        [Fact]
        public void CreateFoodTest()
        {
            var newFood = new FoodDTO
            {
                Name = "TESTFOOD4",
                Price = 1000,
                Spicy = true,
                Vegetarian = false,
                CategoryId = _categoryDTOs[0].Id,
                Description = "asdffgh"
            };

            var controller = new FoodsController(_context);
            var result = controller.PostFood(newFood);

            // Assert
            var objectResult = Assert.IsType<CreatedAtRouteResult>(result);
            var model = Assert.IsAssignableFrom<FoodDTO>(objectResult.Value);
            Assert.Equal(_foodDTOs.Count + 1, _context.Foods.Count());
            Assert.Equal(newFood, model);
        }

        [Fact]
        public void PutFoodTest()
        {
            var newFood = new FoodDTO
            {
                Id = _foodDTOs[2].Id,
                Name = "TESTFOOD4",
                Price = 1000,
                Spicy = true,
                Vegetarian = false,
                CategoryId = _categoryDTOs[0].Id,
                Description = "asdffgh"
            };

            var controller = new FoodsController(_context);
            var result = controller.PutFood(newFood);

            // Assert
            Assert.IsType<OkResult>(result);
            Assert.Equal(_foodDTOs.Count, _context.Foods.Count());
        }

        [Fact]
        public void DeleteFoodTest()
        {
            var controller = new FoodsController(_context);
            var result = controller.DeleteFood(_foodDTOs[2].Id);

            // Assert
            Assert.IsType<OkResult>(result);
            Assert.Equal(_foodDTOs.Count -1, _context.Foods.Count());
        }
    }
}


