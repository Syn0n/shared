﻿using FoodService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace FoodService.Admin.Persistence
{
    class FoodServicePersistence : IFoodServicePersistence
    {
        private HttpClient _client;

        public FoodServicePersistence(String baseAddress)
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(baseAddress);
        }

        public async Task<IEnumerable<FoodDTO>> ReadFoodsAsync()
        {
            try
            {
                // a kéréseket a kliensen keresztül végezzük
                HttpResponseMessage response = await _client.GetAsync("api/foods/");
                if (response.IsSuccessStatusCode) // amennyiben sikeres a művelet
                {
                    IEnumerable<FoodDTO> foods = await response.Content.ReadAsAsync<IEnumerable<FoodDTO>>();
                    // a tartalmat JSON formátumból objektumokká alakítjuk

                    // képek lekérdezése:
                    foreach (FoodDTO food in foods)
                    {
                        response = await _client.GetAsync("api/images/" + food.Id);
                        if (response.IsSuccessStatusCode)
                        {
                            food.Images = (await response.Content.ReadAsAsync<IEnumerable<ImageDTO>>()).ToList();
                        }
                    }

                    return foods;
                }
                else
                {
                    throw new PersistenceUnavailableException("Service returned response: " + response.StatusCode);
                }
            }
            catch (Exception ex)
            {
                throw new PersistenceUnavailableException(ex);
            }
        }

        public async Task<IEnumerable<OrderDTO>> ReadOrdersAsync()
        {
            try
            {
                // a kéréseket a kliensen keresztül végezzük
                HttpResponseMessage response = await _client.GetAsync("api/orders/");
                if (response.IsSuccessStatusCode) // amennyiben sikeres a művelet
                {
                    IEnumerable<OrderDTO> orders = await response.Content.ReadAsAsync<IEnumerable<OrderDTO>>();
                    // a tartalmat JSON formátumból objektumokká alakítjuk

                    foreach (OrderDTO order in orders)
                    {
                        response = await _client.GetAsync("api/orders/" + order.Id);
                        if (response.IsSuccessStatusCode)
                        {
                            order.OrderedFoods = (await response.Content.ReadAsAsync<IEnumerable<FoodDTO>>()).ToList();
                        }
                    }

                    return orders;
                }
                else
                {
                    throw new PersistenceUnavailableException("Service returned response: " + response.StatusCode);
                }
            }
            catch (Exception ex)
            {
                throw new PersistenceUnavailableException(ex);
            }
        }

        public async Task<IEnumerable<CategoryDTO>> ReadCategoriesAsync()
        {
            try
            {
                HttpResponseMessage response = await _client.GetAsync("api/categories/");
                if (response.IsSuccessStatusCode)
                {
                    IEnumerable<CategoryDTO> categories = await response.Content.ReadAsAsync<IEnumerable<CategoryDTO>>();

                    return categories;
                }
                else
                {
                    throw new PersistenceUnavailableException("Service returned response: " + response.StatusCode);
                }
            }
            catch (Exception ex)
            {
                throw new PersistenceUnavailableException(ex);
            }
        }

        public async Task<Boolean> CreateFoodAsync(FoodDTO food)
        {
            try
            {
                HttpResponseMessage response = await _client.PostAsJsonAsync("api/foods/", food); // az értékeket azonnal JSON formátumra alakítjuk
                food.Id = (await response.Content.ReadAsAsync<FoodDTO>()).Id; // a válaszüzenetben megkapjuk a végleges azonosítót
                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                throw new PersistenceUnavailableException(ex);
            }
        }

        public async Task<Boolean> UpdateFoodAsync(FoodDTO food)
        {
            try
            {
                HttpResponseMessage response = await _client.PutAsJsonAsync("api/foods/", food);
                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                throw new PersistenceUnavailableException(ex);
            }
        }

        public async Task<Boolean> UpdateOrderAsync(OrderDTO order)
        {
            try
            {
                HttpResponseMessage response = await _client.PutAsJsonAsync("api/orders/", order);
                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                throw new PersistenceUnavailableException(ex);
            }
        }

        public async Task<Boolean> DeleteFoodAsync(FoodDTO food)
        {
            try
            {
                HttpResponseMessage response = await _client.DeleteAsync("api/foods/" + food.Id);
                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                throw new PersistenceUnavailableException(ex);
            }
        }

        public async Task<Boolean> CreateFoodImageAsync(ImageDTO image)
        {
            try
            {
                HttpResponseMessage response = await _client.PostAsJsonAsync("api/images/", image); // elküldjük a képet
                if (response.IsSuccessStatusCode)
                {
                    image.Id = await response.Content.ReadAsAsync<Int32>(); // a válaszüzenetben megkapjuk az azonosítót
                }
                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                throw new PersistenceUnavailableException(ex);
            }
        }

        public async Task<Boolean> DeleteFoodImageAsync(ImageDTO image)
        {
            try
            {
                HttpResponseMessage response = await _client.DeleteAsync("api/images/" + image.Id);
                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                throw new PersistenceUnavailableException(ex);
            }
        }

        public async Task<Boolean> LoginAsync(String userName, String userPassword)
        {
            try
            {
                HttpResponseMessage response = await _client.GetAsync("api/account/login/" + userName + "/" + userPassword);
                return response.IsSuccessStatusCode; // a művelet eredménye megadja a bejelentkezés sikeressségét
            }
            catch (Exception ex)
            {
                throw new PersistenceUnavailableException(ex);
            }
        }

        public async Task<Boolean> LogoutAsync()
        {
            try
            {
                HttpResponseMessage response = await _client.GetAsync("api/account/logout");
                return !response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                throw new PersistenceUnavailableException(ex);
            }
        }
    }
}
