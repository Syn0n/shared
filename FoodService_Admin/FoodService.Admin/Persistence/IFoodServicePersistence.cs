﻿using FoodService.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FoodService.Admin.Persistence
{
    public interface IFoodServicePersistence
    {
        Task<IEnumerable<FoodDTO>> ReadFoodsAsync();
        Task<IEnumerable<OrderDTO>> ReadOrdersAsync();
        Task<IEnumerable<CategoryDTO>> ReadCategoriesAsync();
        Task<Boolean> CreateFoodAsync(FoodDTO food);
        Task<Boolean> UpdateFoodAsync(FoodDTO food);
        Task<Boolean> UpdateOrderAsync(OrderDTO food);
        Task<Boolean> DeleteFoodAsync(FoodDTO food);
        Task<Boolean> CreateFoodImageAsync(ImageDTO image);
        Task<Boolean> DeleteFoodImageAsync(ImageDTO image);
        Task<Boolean> LoginAsync(String userName, String userPassword);
        Task<Boolean> LogoutAsync();
    }
}
