﻿using System;

namespace FoodService.Admin.Model
{
    public class OrderEventArgs : EventArgs
    {
        public Int32 OrderId { get; set; }
    }
}
