﻿using System;

namespace FoodService.Admin.Model
{
    public class FoodEventArgs : EventArgs
    {
        public Int32 FoodId { get; set; }
    }
}
