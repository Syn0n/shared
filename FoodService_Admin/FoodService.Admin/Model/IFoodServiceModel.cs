﻿using FoodService.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FoodService.Admin.Model
{
    public interface IFoodServiceModel
    {
        IReadOnlyList<OrderDTO> Orders { get; }
        IReadOnlyList<FoodDTO> Foods { get; }
        IReadOnlyList<CategoryDTO> Categories { get; }

        Boolean IsUserLoggedIn { get; }
        event EventHandler<FoodEventArgs> FoodChanged;
        event EventHandler<OrderEventArgs> OrderChanged;

        void CreateFood(FoodDTO food);
        void CreateImage(Int32 foodId, Byte[] imageSmall, Byte[] imageLarge);
        void UpdateFood(FoodDTO food);
        void UpdateOrder(OrderDTO order);
        void DeleteFood(FoodDTO food);
        void DeleteImage(ImageDTO image);
        
        Task LoadAsync();
        Task SaveAsync();
        Task<Boolean> LoginAsync(String userName, String userPassword);
        Task<Boolean> LogoutAsync();
    }
}
