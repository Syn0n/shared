﻿using FoodService.Admin.Persistence;
using FoodService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodService.Admin.Model
{
    class FoodServiceModel : IFoodServiceModel
    {
        private enum DataFlag
        {
            Create,
            Update,
            Delete
        }

        private IFoodServicePersistence _persistence;
        private List<FoodDTO> _foods;
        private List<CategoryDTO> _categories;
        private Dictionary<FoodDTO, DataFlag> _foodFlags;
        private Dictionary<OrderDTO, DataFlag> _orderFlags;
        private Dictionary<ImageDTO, DataFlag> _imageFlags;
        private List<OrderDTO> _orders;

        public FoodServiceModel(IFoodServicePersistence persistence)
        {
            if (persistence == null)
                throw new ArgumentNullException(nameof(persistence));

            IsUserLoggedIn = false;
            _persistence = persistence;
        }

        public IReadOnlyList<OrderDTO> Orders { get { return _orders; } }
        public IReadOnlyList<FoodDTO> Foods { get { return _foods; } }
        public IReadOnlyList<CategoryDTO> Categories { get { return _categories; } }

        public Boolean IsUserLoggedIn { get; private set; }

        public event EventHandler<FoodEventArgs> FoodChanged;
        public event EventHandler<OrderEventArgs> OrderChanged;

        public void CreateFood(FoodDTO food)
        {
            if (food == null)
                throw new ArgumentNullException(nameof(food));

            if (_foods.Contains(food))
                throw new ArgumentException("The food is already in the collection.", nameof(food));

            food.Id = (_foods.Count > 0 ? _foods.Max(f => f.Id) : 0) + 1;
            _foodFlags.Add(food, DataFlag.Create);
            _foods.Add(food);
        }

        public void CreateImage(Int32 foodId, Byte[] imageSmall, Byte[] imageLarge)
        {
            FoodDTO food = _foods.FirstOrDefault(b => b.Id == foodId);
            if (food == null)
                throw new ArgumentException("The food does not exist.", nameof(foodId));

            // létrehozzuk a képet
            ImageDTO image = new ImageDTO()
            {
                Id = _foods.Max(f => f.Images.Any() ? f.Images.Max(im => im.Id) : 0) + 1,
                FoodId = foodId,
                ImageSmall = imageSmall,
                ImageLarge = imageLarge
            };

            // hozzáadjuk
            food.Images.Add(image);
            _imageFlags.Add(image, DataFlag.Create);

            // jellezzük a változást
            OnFoodChanged(food.Id);
        }

        public void UpdateFood(FoodDTO food)
        {
            if (food == null)
                throw new ArgumentNullException(nameof(food));

            // keresés azonosító alapján
            FoodDTO foodToModify = _foods.FirstOrDefault(f => f.Id == food.Id);

            if (foodToModify == null)
                throw new ArgumentException("The food does not exist.", nameof(food));

            // módosítások végrehajtása
            foodToModify.Category = food.Category;         
            foodToModify.Name = food.Name;
            foodToModify.Price = food.Price;
            foodToModify.Spicy = food.Spicy;
            foodToModify.Vegetarian = food.Vegetarian;
            foodToModify.Description = food.Description;

            switch (food.Category.Name[0])
            {
                case 'S': foodToModify.CategoryId = 1; break;
                case 'D': foodToModify.CategoryId = 3; break;
                case 'P': foodToModify.CategoryId = 2; break;
            }

            // külön állapottal jelezzük, ha egy adat újonnan hozzávett
            if (_foodFlags.ContainsKey(foodToModify) && _foodFlags[foodToModify] == DataFlag.Create)
            {
                _foodFlags[foodToModify] = DataFlag.Create;
            }
            else
            {
                _foodFlags[foodToModify] = DataFlag.Update;
            }
            // jelezzük a változást
            OnFoodChanged(food.Id);
        }

        public void UpdateOrder(OrderDTO order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            OrderDTO orderToModify = _orders.FirstOrDefault(o => o.Id == order.Id);

            if (orderToModify == null)
                throw new ArgumentException("The order does not exist.", nameof(order));

            if (orderToModify.DoneDate == DateTime.MinValue && order.Done == false)
            {
                orderToModify.DoneDate = DateTime.Now;
                orderToModify.Done = true;
                _orderFlags[orderToModify] = DataFlag.Update;
                OnOrderChanged(order.Id);
            }
        }

        public void DeleteFood(FoodDTO food)
        {
        if (food == null)
            throw new ArgumentNullException(nameof(food));

        // keresés azonosító alapján
        FoodDTO foodToDelete = _foods.FirstOrDefault(f => f.Id == food.Id);

        if (foodToDelete == null)
            throw new ArgumentException("The food does not exist.", nameof(food));

        // külön kezeljük, ha egy adat újonnan hozzávett (ekkor nem kell törölni a szerverről)
        if (_foodFlags.ContainsKey(foodToDelete) && _foodFlags[foodToDelete] == DataFlag.Create)
            _foodFlags.Remove(foodToDelete);
        else
            _foodFlags[foodToDelete] = DataFlag.Delete;

        _foodFlags.Remove(foodToDelete);
        }

        public void DeleteImage(ImageDTO image)
        {
            if (image == null)
                throw new ArgumentNullException(nameof(image));

            // megkeressük a képet
            foreach (FoodDTO food in _foods)
            {
                if (!food.Images.Contains(image))
                    continue;

                // kezeljük az állapotjelzéseket is
                if (_imageFlags.ContainsKey(image))
                    _imageFlags.Remove(image);
                else
                    _imageFlags.Add(image, DataFlag.Delete);

                // töröljük a képet
                food.Images.Remove(image);

                // jellezzük a változást
                OnFoodChanged(food.Id);

                return;
            }

            // ha idáig eljutott, akkor nem sikerült képet törölni+
            throw new ArgumentException("The image does not exist.", nameof(image));
        }

        public async Task LoadAsync()
        {
            // adatok
            _foods = (await _persistence.ReadFoodsAsync()).ToList();
            _orders = (await _persistence.ReadOrdersAsync()).ToList();
            _categories = (await _persistence.ReadCategoriesAsync()).ToList();

            // állapotjelzések
            _foodFlags = new Dictionary<FoodDTO, DataFlag>();
            _orderFlags = new Dictionary<OrderDTO, DataFlag>();
            _imageFlags = new Dictionary<ImageDTO, DataFlag>();
        }

        public async Task SaveAsync()
        {
            List<FoodDTO> foodsToSave = _foodFlags.Keys.ToList();
            List<OrderDTO> ordersToSave = _orderFlags.Keys.ToList();

            foreach (FoodDTO food in foodsToSave)
            {
                Boolean result = true;

                // az állapotjelzőnek megfelelő műveletet végezzük el
                switch (_foodFlags[food])
                {
                    case DataFlag.Create:
                        result = await _persistence.CreateFoodAsync(food);
                        break;
                    case DataFlag.Delete:
                        result = await _persistence.DeleteFoodAsync(food);
                        break;
                    case DataFlag.Update:
                        result = await _persistence.UpdateFoodAsync(food);
                        break;
                }

                if (!result)
                    throw new InvalidOperationException("Operation " + _foodFlags[food] + " failed on food " + food.Id);

                // ha sikeres volt a mentés, akkor törölhetjük az állapotjelzőt
                _foodFlags.Remove(food);
            }

            foreach (OrderDTO order in ordersToSave)
            {
                Boolean result = true;

                // az állapotjelzőnek megfelelő műveletet végezzük el
                if (_orderFlags[order] == DataFlag.Update)
                {
                        result = await _persistence.UpdateOrderAsync(order);
                }

                if (!result)
                    throw new InvalidOperationException("Operation " + _orderFlags[order] + " failed on order " + order.Id);

                // ha sikeres volt a mentés, akkor törölhetjük az állapotjelzőt
                _orderFlags.Remove(order);
            }

            List<ImageDTO> imagesToSave = _imageFlags.Keys.ToList();

            foreach (ImageDTO image in imagesToSave)
            {
                Boolean result = true;

                switch (_imageFlags[image])
                {
                    case DataFlag.Create:
                        result = await _persistence.CreateFoodImageAsync(image);
                        break;
                    case DataFlag.Delete:
                        result = await _persistence.DeleteFoodImageAsync(image);
                        break;
                }

                if (!result)
                    throw new InvalidOperationException("Operation " + _imageFlags[image] + " failed on image " + image.Id);

                // ha sikeres volt a mentés, akkor törölhetjük az állapotjelzőt
                _imageFlags.Remove(image);
            }
        }

        public async Task<Boolean> LoginAsync(String userName, String userPassword)
        {
            IsUserLoggedIn = await _persistence.LoginAsync(userName, userPassword);
            return IsUserLoggedIn;
        }

        public async Task<Boolean> LogoutAsync()
        {
            if (!IsUserLoggedIn)
                return true;

            IsUserLoggedIn = !(await _persistence.LogoutAsync());

            return IsUserLoggedIn;
        }

        private void OnFoodChanged(Int32 foodId)
        {
            if (FoodChanged != null)
                FoodChanged(this, new FoodEventArgs { FoodId = foodId });
        }

        private void OnOrderChanged(Int32 orderId)
        {
            if (OrderChanged != null)
                OrderChanged(this, new OrderEventArgs { OrderId = orderId });
        }
    }
}
