﻿using FoodService.Admin.Model;
using FoodService.Admin.Persistence;
using FoodService.Data;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace FoodService.Admin.ViewModel
{
    class MainViewModel : ViewModelBase
    {
        private IFoodServiceModel _model;
        private ObservableCollection<FoodDTO> _foods;
        private ObservableCollection<CategoryDTO> _categories;
        private ObservableCollection<OrderDTO> _orders;
        private Boolean _isLoaded;
        private Boolean _isLoadedOrder;
        private FoodDTO _selectedFood;
        private OrderDTO _selectedOrder;

        private const Int32 All = 0;
        private const Int32 OnlyOrdered = 1;
        private const Int32 OnlyDone = 2;

        public ObservableCollection<FoodDTO> Foods
        {
            get { return _foods; }
            private set
            {
                if (_foods != value)
                {
                    _foods = value;
                    OnPropertyChanged();
                }
            }
        }

        public ObservableCollection<CategoryDTO> Categories
        {
            get { return _categories; }
            private set
            {
                if (_categories != value)
                {
                    _categories = value;
                    OnPropertyChanged();
                }
            }
        }

        public ObservableCollection<OrderDTO> Orders
        {
            get { return _orders; }
            private set
            {
                if (_orders != value)
                {
                    _orders = value;
                    OnPropertyChanged();
                }
            }
        }

        public Boolean IsLoaded
        {
            get { return _isLoaded; }
            private set
            {
                if (_isLoaded != value)
                {
                    _isLoaded = value;
                    OnPropertyChanged();
                }
            }
        }

        public Boolean IsLoadedOrder
        {
            get { return _isLoadedOrder; }
            private set
            {
                if (_isLoadedOrder != value)
                {
                    _isLoadedOrder = value;
                    OnPropertyChanged();
                }
            }
        }

        public FoodDTO SelectedFood
        {
            get { return _selectedFood; }
            set
            {
                if (_selectedFood != value)
                {
                    _selectedFood = value;
                    OnPropertyChanged();
                }
            }
        }

        public OrderDTO SelectedOrder
        {
            get { return _selectedOrder; }
            set
            {
                if (_selectedOrder != value)
                {
                    _selectedOrder = value;
                    OnPropertyChanged();
                }
            }
        }

        public FoodDTO EditedFood { get; private set; }
        public OrderDTO EditedOrder { get; private set; }
        public DelegateCommand FilterOrdersCommand { get; private set; }
        public DelegateCommand FilterOrdersByNameCommand { get; private set; }
        public DelegateCommand FilterOrdersByAddressCommand { get; private set; }
        public DelegateCommand CreateFoodCommand { get; private set; }
        public DelegateCommand CreateImageCommand { get; private set; }
        public DelegateCommand UpdateFoodCommand { get; private set; }
        public DelegateCommand UpdateOrderCommand { get; private set; }
        public DelegateCommand DeleteFoodCommand { get; private set; }
        public DelegateCommand DeleteImageCommand { get; private set; }
        public DelegateCommand SaveChangesCommand { get; private set; }
        public DelegateCommand CancelChangesCommand { get; private set; }
        public DelegateCommand LogoutCommand { get; private set; }
        public DelegateCommand ExitCommand { get; private set; }
        public DelegateCommand LoadCommand { get; private set; }
        public DelegateCommand SaveCommand { get; private set; }

        public event EventHandler ExitApplication;
        public event EventHandler Logout;
        public event EventHandler FoodEditingStarted;
        public event EventHandler FoodEditingFinished;
        public event EventHandler<FoodEventArgs> ImageEditingStarted;

        public MainViewModel(IFoodServiceModel model)
        {
            _model = model;
            _isLoaded = false;
            _isLoadedOrder = false;
            _model.FoodChanged += Model_FoodChanged;
            _model.OrderChanged += Model_OrderChanged;

            FilterOrdersCommand = new DelegateCommand(param => FilterOrders(param as String));
            FilterOrdersByNameCommand = new DelegateCommand(param => FilterOrdersByName(param as String));
            FilterOrdersByAddressCommand = new DelegateCommand(param => FilterOrdersByAddress(param as String));
            CreateFoodCommand = new DelegateCommand(param =>
            {
                EditedFood = new FoodDTO();  // a szerkesztett kaja új lesz
                OnFoodEditingStarted();
            });
            CreateImageCommand = new DelegateCommand(param => OnImageEditingStarted(param == null ? -1 : (param as FoodDTO).Id));
            UpdateFoodCommand = new DelegateCommand(param => UpdateFood(param as FoodDTO));
            UpdateOrderCommand = new DelegateCommand(param => UpdateOrder(param as OrderDTO));
            DeleteFoodCommand = new DelegateCommand(param => DeleteFood(param as FoodDTO));
            DeleteImageCommand = new DelegateCommand(param => DeleteImage(param as ImageDTO));
            SaveChangesCommand = new DelegateCommand(param => SaveChanges());
            CancelChangesCommand = new DelegateCommand(param => CancelChanges());
            LoadCommand = new DelegateCommand(param => LoadAsync());
            SaveCommand = new DelegateCommand(param => SaveAsync());
            LogoutCommand = new DelegateCommand(param => OnLogout());
            ExitCommand = new DelegateCommand(param => OnExitApplication());
        }

        private void FilterOrders(String filternumber)
        {
            switch (Convert.ToInt32(filternumber))
            {
                case All: Orders = new ObservableCollection<OrderDTO>(_model.Orders); break;
                case OnlyOrdered: Orders = new ObservableCollection<OrderDTO>(_model.Orders.Where(o => o.Done == false)); break;
                case OnlyDone: Orders = new ObservableCollection<OrderDTO>(_model.Orders.Where(o => o.Done == true)); break;
            }
        }

        private void FilterOrdersByName(String searchString)
        {
            if (!String.IsNullOrEmpty(searchString))
            {
                Orders = new ObservableCollection<OrderDTO>(Orders.Where(o => o.Name.Contains(searchString)));
            }
        }

        private void FilterOrdersByAddress(String searchString)
        {
            if (!String.IsNullOrEmpty(searchString))
            {
                Orders = new ObservableCollection<OrderDTO>(Orders.Where(o => o.BillingAddress.Contains(searchString)));
            }
        }

        private void UpdateFood(FoodDTO food)
        {
            if (food == null)
                return;

            EditedFood = new FoodDTO
            {
                Id = food.Id,
                Name = food.Name,
                Category = food.Category,
                Price = food.Price,
                Spicy = food.Spicy,
                Vegetarian = food.Vegetarian,
                Description = food.Description
            }; // a szerkesztett kaja adatait áttöltjük a kijelöltből

            OnFoodEditingStarted();
        }

        private void UpdateOrder(OrderDTO order)
        {
            if (order == null)
                return;

            EditedOrder = new OrderDTO
            {
                Id = order.Id,
                Name = order.Name,
                BillingAddress = order.BillingAddress,
                TotalPrice = order.TotalPrice,
                PhoneNumber = order.PhoneNumber,
                OrderedDate = order.OrderedDate,
                DoneDate = order.DoneDate
            }; // a szerkesztett kaja adatait áttöltjük a kijelöltből
            _model.UpdateOrder(EditedOrder);
        }

        private void DeleteFood(FoodDTO food)
        {
            if (food == null || !Foods.Contains(food))
                return;

            Foods.Remove(food);

            _model.DeleteFood(food);
        }

        private void DeleteImage(ImageDTO image)
        {
            if (image == null)
                return;

            _model.DeleteImage(image);
        }

        private void SaveChanges()
        {
            // ellenőrzések
            if (String.IsNullOrEmpty(EditedFood.Name))
            {
                OnMessageApplication("A kajanév nincs megadva!");
                return;
            }
            if (EditedFood.Spicy != true && EditedFood.Spicy != false)
            {
                OnMessageApplication("A csípősség nincs megadva!");
                return;
            }
            if (EditedFood.Vegetarian != true && EditedFood.Vegetarian != false)
            {
                OnMessageApplication("A vega osztály nincs megadva!");
                return;
            }
            
            if (EditedFood.Category == null)
            {
                OnMessageApplication("A kategória nincs megadva!");
                return;
            }
            switch (EditedFood.Category.Name[0])
            {
                case 'S': EditedFood.CategoryId = 1; break;
                case 'D': EditedFood.CategoryId = 3; break;
                case 'P': EditedFood.CategoryId = 2; break;
            }
            if (EditedFood.Price == 0)
            {
                OnMessageApplication("Az ár nincs megadva!");
                return;
            }

            // mentés
            if (EditedFood.Id == 0) // ha új a kaja
            {
                try
                {
                    _model.CreateFood(EditedFood);
                }
                catch(ArgumentException)
                {
                    OnMessageApplication("A kaja már létezik!");
                    return;
                }
                Foods.Add(EditedFood);
                SelectedFood = EditedFood;
            }
            else // ha már létezik a kaja
            {
                _model.UpdateFood(EditedFood);
            }

            EditedFood = null;
            EditedOrder = null;

            OnFoodEditingFinished();
        }

        private void CancelChanges()
        {
            EditedFood = null;
            OnFoodEditingFinished();
        }

        private async void LoadAsync()
        {
            try
            {
                await _model.LoadAsync();
                Foods = new ObservableCollection<FoodDTO>(_model.Foods); // az adatokat egy követett gyűjteménybe helyezzük
                Categories = new ObservableCollection<CategoryDTO>(_model.Categories);
                Orders = new ObservableCollection<OrderDTO>(_model.Orders);
                IsLoaded = true;
                IsLoadedOrder = true;
            }
            catch (PersistenceUnavailableException)
            {
                OnMessageApplication("A betöltés sikertelen! Nincs kapcsolat a kiszolgálóval.");
            }
        }

        private async void SaveAsync()
        {
            try
            {
                await _model.SaveAsync();
                OnMessageApplication("A mentés sikeres!");
            }
            catch (PersistenceUnavailableException)
            {
                OnMessageApplication("A mentés sikertelen! Nincs kapcsolat a kiszolgálóval.");
            }
        }

        private void Model_FoodChanged(object sender, FoodEventArgs e)
        {
            Int32 index = Foods.IndexOf(Foods.FirstOrDefault(food => food.Id == e.FoodId));
            Foods.RemoveAt(index); // módosítjuk a kollekciót
            Foods.Insert(index, _model.Foods[index]);

            SelectedFood = Foods[index]; // és az aktuális épületet
        }

        private void Model_OrderChanged(object sender, OrderEventArgs e)
        {
            Int32 index = Orders.IndexOf(Orders.FirstOrDefault(order => order.Id == e.OrderId));
            Orders.RemoveAt(index); // módosítjuk a kollekciót
            Orders.Insert(index, _model.Orders[index]);

            SelectedOrder = Orders[index]; // és az aktuális épületet
        }

        private void OnExitApplication()
        {
            if (ExitApplication != null)
                ExitApplication(this, EventArgs.Empty);
        }

        private void OnLogout()
        {
            if (Logout != null)
                Logout(this, EventArgs.Empty);
        }

        private void OnFoodEditingStarted()
        {
            if (FoodEditingStarted != null)
                FoodEditingStarted(this, EventArgs.Empty);
        }

        private void OnFoodEditingFinished()
        {
            if (FoodEditingFinished != null)
                FoodEditingFinished(this, EventArgs.Empty);
        }

        private void OnImageEditingStarted(Int32 foodId)
        {
            if (ImageEditingStarted != null)
                ImageEditingStarted(this, new FoodEventArgs { FoodId = foodId });
        }
    }
}
