﻿using FoodService.Data;
using FoodService.Service.Models;
using System;
using System.Linq;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FoodService.Service.Controllers
{
    [Route("api/[controller]")]
    public class CategoriesController : Controller
    {
        private FoodServiceContext _context;


        public CategoriesController(FoodServiceContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            _context = context;
        }

        [HttpGet]
        public IActionResult GetCategories()
        {
            try
            {
                return Ok(_context.Categories.ToList().Select(category => new CategoryDTO
                {
                    Id = category.Id,
                    Name = category.Name
                }));
            }
            catch
            {
                // Internal Server Error
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
