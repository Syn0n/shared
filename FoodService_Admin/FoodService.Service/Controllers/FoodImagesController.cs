﻿using FoodService.Data;
using FoodService.Service.Models;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace FoodService.Service.Controllers
{
    [Route("api/images")]
    public class FoodImagesController : Controller
    {
        private readonly FoodServiceContext _context;

        /// <summary>
        /// Vezérlő példányosítása.
        /// </summary>
        /// <param name="context">Entitásmodell.</param>
        public FoodImagesController(FoodServiceContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            _context = context;
        }

        [HttpGet("{foodId}")]
        public IActionResult GetImages(Int32 foodId)
        {
            // a megfelelő képeket betöltjük és átalakítjuk (csak a kis képeket)
            return Ok(_context.FoodImages.Where(image => image.FoodId == foodId).Select(image => new ImageDTO { Id = image.Id, ImageSmall = image.ImageSmall }));
        }

        [HttpPost] // itt nem kell paramétereznünk, csak jelezzük, hogy az egyedi útvonalat vesszük igénybe
        [Authorize(Roles = "administrator")] // csak bejelentkezett adminisztrátoroknak
        public IActionResult PostImage([FromBody] ImageDTO image)
        {
            if (image == null || !_context.Foods.Any(building => image.FoodId == building.Id))
                return NotFound();

            FoodImage foodImage = new FoodImage
            {
                FoodId = image.FoodId,
                ImageSmall = image.ImageSmall,
                ImageLarge = image.ImageLarge
            };

            _context.FoodImages.Add(foodImage);

            try
            {
                _context.SaveChanges();
                return Created(Request.GetUri() + image.Id.ToString(), image.Id); // csak az azonosítót küldjük vissza
            }
            catch
            {
                // Internal Server Error
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Kép törlése.
        /// </summary>
        /// <param name="id">A kép azonosítója.</param>
        [Route("{id}")]
        [Authorize(Roles = "administrator")]
        public IActionResult DeleteImage(Int32 id)
        {
            FoodImage image = _context.FoodImages.FirstOrDefault(im => im.Id == id);

            if (image == null)
                return NotFound();

            try
            {
                _context.FoodImages.Remove(image);
                _context.SaveChanges();
                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
