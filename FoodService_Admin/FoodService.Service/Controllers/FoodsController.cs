﻿using FoodService.Data;
using FoodService.Service.Models;
using System;
using System.Linq;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FoodService.Service.Controllers
{
    [Route("api/[controller]")]
    public class FoodsController : Controller
    {
        private FoodServiceContext _context;


        public FoodsController(FoodServiceContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            _context = context;
        }

        [HttpGet]
        public IActionResult GetFoods()
        {
            try
            {
                return Ok(_context.Foods.Include(f => f.Category).ToList().Select(food => new FoodDTO
                {
                    Id = food.Id,
                    Name = food.Name,
                    Category = new CategoryDTO { Id = food.Category.Id, Name = food.Category.Name },
                    CategoryId = food.CategoryId,
                    Price = food.Price,
                    Spicy = food.Spicy,
                    Vegetarian = food.Vegetarian,
                    Description = food.Description
                }));
            }
            catch
            {
                // Internal Server Error
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        
        [HttpGet("{id}")]
        public IActionResult GetFoodForCategory(Int32 id)
        {
            try
            {
                return Ok(_context.Foods.Where(f => f.CategoryId == id).ToList().Select(food => new FoodDTO
                {
                    Id = food.Id,
                    Name = food.Name,
                    Category = new CategoryDTO { Id = food.Category.Id, Name = food.Category.Name },
                    CategoryId = food.CategoryId,
                    Price = food.Price,
                    Spicy = food.Spicy,
                    Vegetarian = food.Vegetarian,
                    Description = food.Description
                }));
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [Authorize(Roles = "administrator")]
        public IActionResult PostFood([FromBody] FoodDTO foodDTO)
        {
            try
            {
                var addedFood = _context.Foods.Add(new Food
                {
                    Name = foodDTO.Name,
                    CategoryId = foodDTO.CategoryId,
                    Price = foodDTO.Price,
                    Spicy = foodDTO.Spicy,
                    Vegetarian = foodDTO.Vegetarian,
                    Description = foodDTO.Description
                });

                _context.SaveChanges();

                foodDTO.Id = addedFood.Entity.Id;

                // visszaküldjük a létrehozott kaját
                return CreatedAtRoute("GetFood", new { id = addedFood.Entity.Id }, foodDTO);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut]
        [Authorize(Roles = "administrator")]
        public IActionResult PutFood([FromBody] FoodDTO foodDTO)
        {
            try
            {
                Food food = _context.Foods.FirstOrDefault(f => f.Id == foodDTO.Id);

                if (food == null)
                    return NotFound();

                food.Name = foodDTO.Name;
                food.CategoryId = foodDTO.CategoryId;
                food.Price = foodDTO.Price;
                food.Spicy = foodDTO.Spicy;
                food.Vegetarian = foodDTO.Vegetarian;
                food.Description = foodDTO.Description;

                _context.SaveChanges();

                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "administrator")]
        public IActionResult DeleteFood(Int32 id)
        {
            try
            {
                Food food = _context.Foods.FirstOrDefault(f => f.Id == id);

                if (food == null)
                    return NotFound();

                _context.Foods.Remove(food);

                _context.SaveChanges();

                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
