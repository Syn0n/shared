﻿using FoodService.Data;
using FoodService.Service.Models;
using System.Collections.Generic;
using System;
using System.Linq;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FoodService.Service.Controllers
{
    [Route("api/[controller]")]
    public class OrdersController : Controller
    {
        private FoodServiceContext _context;


        public OrdersController(FoodServiceContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            _context = context;
        }

        [HttpGet]
        public IActionResult GetOrders()
        {

            var orderedbaskets = _context.Baskets.Where(f => f.Ordered == true);

            try
            {
                return Ok(orderedbaskets.ToList().Select(order => new OrderDTO
                {
                    Id = order.Id,
                    Name = order.Name,
                    BillingAddress = order.BillingAddress,
                    TotalPrice = order.TotalPrice,
                    OrderedDate = order.OrderedDate,
                    DoneDate = order.DoneDate,
                    PhoneNumber = order.PhoneNumber,
                    Done = order.Done,
                }));
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetFoodForOrder(Int32 id)
        {
            Basket order = _context.Baskets.Where(o => o.Id == id && o.Ordered == true).ToList().FirstOrDefault();

                var foodbaskets = _context.FoodBaskets
                .Include(o => o.Food)
                .Where(foodbasket => foodbasket.BasketId == id && foodbasket.Ordered == true).ToList();

                List<FoodDTO> orderedFoods = new List<FoodDTO>();
                foreach (FoodBasket fb in foodbaskets)
                {
                    Food food = _context.Foods.Include(f => f.Category).Where(f => f.Id == fb.FoodId).FirstOrDefault();

                    orderedFoods.Add(new FoodDTO
                    {
                        Id = food.Id,
                        Name = food.Name,
                        Category = new CategoryDTO { Id = food.Category.Id, Name = food.Category.Name },
                        CategoryId = food.CategoryId,
                        Price = food.Price,
                        Spicy = food.Spicy,
                        Vegetarian = food.Vegetarian,
                        Description = food.Description
                    });
                }

            try
            {
                return Ok(orderedFoods);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut]
        [Authorize(Roles = "administrator")]
        public IActionResult PutOrder([FromBody] OrderDTO orderDTO)
        {
            try
            {
                Basket basket = _context.Baskets.FirstOrDefault(b => b.Id == orderDTO.Id);

                if (basket == null)
                    return NotFound();

                basket.Done = true;
                basket.DoneDate = orderDTO.DoneDate;

                _context.SaveChanges();

                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
