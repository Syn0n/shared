﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FoodService.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace FoodService.Service.Models
{
    public class DbInitializer
    {
        private static FoodServiceContext _context;
        private static UserManager<Colleague> _userManager;
        private static RoleManager<IdentityRole<int>> _roleManager;

        public static void Initialize(FoodServiceContext context,
            UserManager<Colleague> userManager, RoleManager<IdentityRole<int>> roleManager,
            string imageDirectory)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;

            _context.Database.EnsureCreated();

            if (_context.Categories.Any())
            {
                return; // Az adatbázis már inicializálva van.
            }

            SeedCategories();
            SeedFoods();
            SeedFoodImages(imageDirectory);
            SeedUsers();
        }

        private static void SeedUsers()
        {
            var adminUser = new Colleague
            {
                UserName = "admin",
                Name = "Adminisztrátor",
                Email = "admin@example.com",
                PhoneNumber = "+36123456789"
            };
            var adminPassword = "Almafa123";
            var adminRole = new IdentityRole<int>("administrator");

            var result1 = _userManager.CreateAsync(adminUser, adminPassword).Result;
            var result2 = _roleManager.CreateAsync(adminRole).Result;
            var result3 = _userManager.AddToRoleAsync(adminUser, adminRole.Name).Result;

            var simpleUser = new Colleague
            {
                UserName = "simple",
                Name = "Simplátor",
                Email = "simple@example.com",
                PhoneNumber = "+36123546789"
            };
            var simplePassword = "Simple123";

            var result4 = _userManager.CreateAsync(simpleUser, simplePassword).Result;
        }

        private static void SeedCategories()
        {

            var categories = new Category[]
            {
                new Category {Name = "Soup"},
                new Category {Name = "Pizza"},
                new Category {Name = "Drink"}
            };
            foreach (Category c in categories)
            {
                _context.Categories.Add(c);
            }

            _context.SaveChanges();
        }

        private static void SeedFoods()
        {
            var foods = new Food[]
            {
                new Food
                {
                    Name = "Chicken Soup",
                    CategoryId = 1,
                    Description = "It's oishy des!",
                    Price = 900,
                    Spicy = false,
                    Vegetarian = false,
                },
                new Food
                {
                    Name = "Potato Soup",
                    CategoryId = 1,
                    Description = "Im not sure it's oishy des.",
                    Price = 900,
                    Spicy = false,
                    Vegetarian = true,
                },
                new Food
                {
                    Name = "Tomato Soup",
                    CategoryId = 1,
                    Description = "Bizarre.",
                    Price = 900,
                    Spicy = true,
                    Vegetarian = true,
                },
                new Food
                {
                    Name = "Vegetarian Soup",
                    CategoryId = 1,
                    Description = "Vegetarian for sure.",
                    Price = 900,
                    Spicy = false,
                    Vegetarian = true,
                },
                new Food
                {
                    Name = "Bean Soup",
                    CategoryId = 1,
                    Description = "Black Bean deadly soup.",
                    Price = 900,
                    Spicy = true,
                    Vegetarian = false,
                },
                new Food
                {
                    Name = "Ham Pizza",
                    CategoryId = 2,
                    Description = "favourite",
                    Price = 1500,
                    Spicy = false,
                    Vegetarian = false,
                },
                new Food
                {
                    Name = "Olive Pizza",
                    CategoryId = 2,
                    Description = "hate it",
                    Price = 1500,
                    Spicy = false,
                    Vegetarian = false,
                },
                new Food
                {
                    Name = "Spicy Pizza",
                    CategoryId = 2,
                    Description = "you dont want to eat it",
                    Price = 1500,
                    Spicy = true,
                    Vegetarian = false,
                },
                new Food
                {
                    Name = "Vegan Pizza",
                    CategoryId = 2,
                    Description = "for all kind of vegans",
                    Price = 1500,
                    Spicy = false,
                    Vegetarian = true,
                },
                new Food
                {
                    Name = "VegetarianPizza",
                    CategoryId = 2,
                    Description = "it is not vegan",
                    Price = 1500,
                    Spicy = false,
                    Vegetarian = true,
                },
                new Food
                {
                    Name = "StrawberryDrink",
                    CategoryId = 3,
                    Description = "sweety",
                    Price = 1100,
                    Spicy = false,
                    Vegetarian = true,
                },
                new Food
                {
                    Name = "Orange Drink",
                    CategoryId = 3,
                    Description = "almost Orange",
                    Price = 1100,
                    Spicy = false,
                    Vegetarian = true,
                },
                new Food
                {
                    Name = "Kawaii Drink",
                    CategoryId = 3,
                    Description = "It's not vegetarian nor spicy",
                    Price = 1100,
                    Spicy = false,
                    Vegetarian = false,
                },
                new Food
                {
                    Name = "Jaeger Coctail",
                    CategoryId = 3,
                    Description = "it has medic effect",
                    Price = 1100,
                    Spicy = true,
                    Vegetarian = true,
                },
                new Food
                {
                    Name = "LimonadeDrink",
                    CategoryId = 3,
                    Description = "without Lime",
                    Price = 1100,
                    Spicy = true,
                    Vegetarian = true,
                },
            };
            foreach (Food f in foods)
            {
                _context.Foods.Add(f);
            }
            _context.SaveChanges();
        }

        private static async void SeedFoodImages(string imageDirectory)
        {
            if (Directory.Exists(imageDirectory))
            {
                var images = new List<FoodImage>();

                var largePath = Path.Combine(imageDirectory, "soup_1.png");
                var smallPath = Path.Combine(imageDirectory, "soup_1_thumb.png");
                if (File.Exists(largePath) && File.Exists(smallPath))
                {
                    images.Add(new FoodImage
                    {
                        FoodId = 1,
                        ImageLarge = await File.ReadAllBytesAsync(largePath),
                        ImageSmall = await File.ReadAllBytesAsync(smallPath)
                    });
                }

                largePath = Path.Combine(imageDirectory, "soup_2.png");
                smallPath = Path.Combine(imageDirectory, "soup_2_thumb.png");
                if (File.Exists(largePath) && File.Exists(smallPath))
                {
                    images.Add(new FoodImage
                    {
                        FoodId = 2,
                        ImageLarge = await File.ReadAllBytesAsync(largePath),
                        ImageSmall = await File.ReadAllBytesAsync(smallPath)
                    });
                }

                largePath = Path.Combine(imageDirectory, "soup_3.png");
                smallPath = Path.Combine(imageDirectory, "soup_3_thumb.png");
                if (File.Exists(largePath) && File.Exists(smallPath))
                {
                    images.Add(new FoodImage
                    {
                        FoodId = 3,
                        ImageLarge = await File.ReadAllBytesAsync(largePath),
                        ImageSmall = await File.ReadAllBytesAsync(smallPath)
                    });
                }

                largePath = Path.Combine(imageDirectory, "soup_4.png");
                smallPath = Path.Combine(imageDirectory, "soup_4_thumb.png");
                if (File.Exists(largePath) && File.Exists(smallPath))
                {
                    images.Add(new FoodImage
                    {
                        FoodId = 4,
                        ImageLarge = await File.ReadAllBytesAsync(largePath),
                        ImageSmall = await File.ReadAllBytesAsync(smallPath)
                    });
                }

                largePath = Path.Combine(imageDirectory, "soup_5.png");
                smallPath = Path.Combine(imageDirectory, "soup_5_thumb.png");
                if (File.Exists(largePath) && File.Exists(smallPath))
                {
                    images.Add(new FoodImage
                    {
                        FoodId = 5,
                        ImageLarge = await File.ReadAllBytesAsync(largePath),
                        ImageSmall = await File.ReadAllBytesAsync(smallPath)
                    });
                }

                largePath = Path.Combine(imageDirectory, "pizza_1.png");
                smallPath = Path.Combine(imageDirectory, "pizza_1_thumb.png");
                if (File.Exists(largePath) && File.Exists(smallPath))
                {
                    images.Add(new FoodImage
                    {
                        FoodId = 6,
                        ImageLarge = await File.ReadAllBytesAsync(largePath),
                        ImageSmall = await File.ReadAllBytesAsync(smallPath)
                    });
                }

                largePath = Path.Combine(imageDirectory, "pizza_2.png");
                smallPath = Path.Combine(imageDirectory, "pizza_2_thumb.png");
                if (File.Exists(largePath) && File.Exists(smallPath))
                {
                    images.Add(new FoodImage
                    {
                        FoodId = 7,
                        ImageLarge = await File.ReadAllBytesAsync(largePath),
                        ImageSmall = await File.ReadAllBytesAsync(smallPath)
                    });
                }

                largePath = Path.Combine(imageDirectory, "pizza_3.png");
                smallPath = Path.Combine(imageDirectory, "pizza_3_thumb.png");
                if (File.Exists(largePath) && File.Exists(smallPath))
                {
                    images.Add(new FoodImage
                    {
                        FoodId = 8,
                        ImageLarge = await File.ReadAllBytesAsync(largePath),
                        ImageSmall = await File.ReadAllBytesAsync(smallPath)
                    });
                }

                largePath = Path.Combine(imageDirectory, "pizza_4.png");
                smallPath = Path.Combine(imageDirectory, "pizza_4_thumb.png");
                if (File.Exists(largePath) && File.Exists(smallPath))
                {
                    images.Add(new FoodImage
                    {
                        FoodId = 9,
                        ImageLarge = await File.ReadAllBytesAsync(largePath),
                        ImageSmall = await File.ReadAllBytesAsync(smallPath)
                    });
                }

                largePath = Path.Combine(imageDirectory, "pizza_5.png");
                smallPath = Path.Combine(imageDirectory, "pizza_5_thumb.png");
                if (File.Exists(largePath) && File.Exists(smallPath))
                {
                    images.Add(new FoodImage
                    {
                        FoodId = 10,
                        ImageLarge = await File.ReadAllBytesAsync(largePath),
                        ImageSmall = await File.ReadAllBytesAsync(smallPath)
                    });
                }

                largePath = Path.Combine(imageDirectory, "drink_1.png");
                smallPath = Path.Combine(imageDirectory, "drink_1_thumb.png");
                if (File.Exists(largePath) && File.Exists(smallPath))
                {
                    images.Add(new FoodImage
                    {
                        FoodId = 11,
                        ImageLarge = await File.ReadAllBytesAsync(largePath),
                        ImageSmall = await File.ReadAllBytesAsync(smallPath)
                    });
                }

                largePath = Path.Combine(imageDirectory, "drink_2.png");
                smallPath = Path.Combine(imageDirectory, "drink_2_thumb.png");
                if (File.Exists(largePath) && File.Exists(smallPath))
                {
                    images.Add(new FoodImage
                    {
                        FoodId = 12,
                        ImageLarge = await File.ReadAllBytesAsync(largePath),
                        ImageSmall = await File.ReadAllBytesAsync(smallPath)
                    });
                }

                largePath = Path.Combine(imageDirectory, "drink_3.png");
                smallPath = Path.Combine(imageDirectory, "drink_3_thumb.png");
                if (File.Exists(largePath) && File.Exists(smallPath))
                {
                    images.Add(new FoodImage
                    {
                        FoodId = 13,
                        ImageLarge = await File.ReadAllBytesAsync(largePath),
                        ImageSmall = await File.ReadAllBytesAsync(smallPath)
                    });
                }

                largePath = Path.Combine(imageDirectory, "drink_4.png");
                smallPath = Path.Combine(imageDirectory, "drink_4_thumb.png");
                if (File.Exists(largePath) && File.Exists(smallPath))
                {
                    images.Add(new FoodImage
                    {
                        FoodId = 14,
                        ImageLarge = await File.ReadAllBytesAsync(largePath),
                        ImageSmall = await File.ReadAllBytesAsync(smallPath)
                    });
                }

                largePath = Path.Combine(imageDirectory, "drink_5.png");
                smallPath = Path.Combine(imageDirectory, "drink_5_thumb.png");
                if (File.Exists(largePath) && File.Exists(smallPath))
                {
                    images.Add(new FoodImage
                    {
                        FoodId = 15,
                        ImageLarge = await File.ReadAllBytesAsync(largePath),
                        ImageSmall = await File.ReadAllBytesAsync(smallPath)
                    });
                }

                foreach (var image in images)
                {
                    _context.FoodImages.Add(image);
                }

                _context.SaveChanges();
            }
        }
    }
}
