﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodService.Service.Models
{
    public class Customer
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String UserName { get; set; }
        public String Address { get; set; }
        public String PhoneNumber { get; set; }
        public String Email { get; set; }
        public byte[] UserPassword { get; set; }
    }
}
