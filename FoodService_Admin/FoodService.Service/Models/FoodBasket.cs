﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodService.Service.Models
{
    public class FoodBasket
    {
        public Basket Basket { get; set; }
        public Int32 BasketId { get; set; }
        public Boolean Ordered { get; set; }
        public Food Food { get; set; }
        public Int32 FoodId { get; set; }
        public Int32 Id { get; set; }
    }
}
