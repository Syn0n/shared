﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace FoodService.Service.Models
{
    public class Colleague : IdentityUser<int>
    {
        public Colleague()
        {
            Baskets = new HashSet<Basket>();
        }
        public String Name { get; set; }
        public ICollection<Basket> Baskets { get; set; }
    }
}
