From Coq Require Import Strings.String.
Definition ident : Type := string.

Inductive aexp : Type :=
| ALit  (n : nat)
| AVar  (x : ident)
| APlus (a1 a2 : aexp).

Definition state : Type := ident -> nat.
Definition X:ident := "X"%string.
Definition Y:ident := "Y"%string.
Definition Z:ident := "Z"%string.
Definition aState : state :=
  fun x =>
    match x with
    | "X"%string => 2
    | "Y"%string => 3
    | _ => 0 (* default value for all (global) variables *)
    end.

Definition update(s:state)(x:ident)(n:nat): state :=
  fun y =>
    if eqb x y then n else s y.
Definition empty : state := fun _ => 0.

(* Small-step operational semantics *)

Reserved Notation "c f=> n" (at level 50). (* final step relation *)
Reserved Notation "c i=> c'" (at level 50). (* intermediate step relation *)
Inductive SOS_fstep : aexp * state -> nat -> Prop :=
| eval_lit n s:
  (ALit n, s) f=> n
| eval_var x s:
  (AVar x, s) f=> s x
| eval_plus_r_f (a2:aexp) (s:state) (i n: nat):
  (a2, s) f=> i
  ->
  (APlus (ALit n) a2, s) f=> (n + i)
| ftrans (c1 c2 : aexp * state)(i : nat) :
         SOS_istep c1 c2 -> SOS_fstep c2 i -> SOS_fstep c1 i
where "c f=> n" := (SOS_fstep c n)
with SOS_istep : aexp * state -> aexp * state -> Prop :=
| eval_plus_l_i a1 a2 s a1':
  (a1, s) i=> (a1', s)
  ->
  ((APlus a1 a2), s) i=> ((APlus a1' a2), s)
| eval_plus_l_f (a1 a2: aexp) (s:state) (i:nat):
  (a1, s) f=> i
  ->
  ((APlus a1 a2), s) i=> ((APlus (ALit i) a2), s)
| eval_plus_r_i (a2 a2': aexp) (s:state) (n:nat):
  (a2, s) i=> (a2', s)
  ->
  ((APlus (ALit n) a2), s) i=> ((APlus (ALit n) a2'), s)
| itrans (c1 c2 c3 : aexp * state) :
         SOS_istep c1 c2 -> SOS_istep c2 c3 -> SOS_istep c1 c3
where "c i=> c'" := (SOS_istep c c').

Example der1 (s : state) : (APlus (ALit 3) (ALit 2) , s) f=> 5.
Proof.
  apply (eval_plus_r_f (ALit 2) s 2 3).
  apply (eval_lit 2 s).
Qed.

Definition s1 := update empty X 1.
Example der2 : (APlus (ALit 3) (APlus (AVar X) (ALit 2)) , s1) f=> 6.
Proof.
  apply (eval_plus_r_f (APlus (AVar X) (ALit 2)) s1 3 3).

  apply (ftrans ((APlus (AVar X) (ALit 2)), s1) ((APlus (ALit 1) (ALit 2)), s1) 3).
  apply (eval_plus_l_f (AVar X) (ALit 2) s1 1).
   
  apply (eval_var X s1).
  
  apply (eval_plus_r_f (ALit 2) s1 2 1).
  apply (eval_lit 2 s1).
Qed.