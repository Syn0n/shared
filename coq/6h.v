From Coq Require Import Strings.String.
Definition ident : Type := string.

Inductive aexp : Type :=
| ALit  (n : nat)
| AVar  (x : ident)
| APlus (a1 a2 : aexp).

Definition state : Type := ident -> nat.

Fixpoint aeval (a: aexp)(s: state) : nat :=
match a with
| ALit n => n
| AVar x => s x
| APlus a1 a2 => aeval a1 s + aeval a2 s
end.

Definition X:ident := "X"%string.
Definition Y:ident := "Y"%string.
Definition Z:ident := "Z"%string.

Fixpoint bclosed (a:aexp) : bool :=
match a with
| ALit _ => true
| AVar _ => false
| APlus a1 a2 => bclosed a1 && bclosed a2
end.

From Coq Require Import Bool.

Inductive closed : aexp -> Prop :=
  Cl_Lit n:
  closed (ALit n)
| Cl_Plus a1 a2:
  closed a1 -> closed a2 -> closed (APlus a1 a2).

(* A state in which all variables are equal to zero. *)
Definition zero_state: state :=
  fun _ => 0.

(* This function replaces every variable with a zero literal *)
Fixpoint zero_exp (a:aexp): aexp :=
match a with
 | ALit _ => a
 | AVar _ => ALit 0
 | APlus a1 a2 => APlus (zero_exp a1) (zero_exp a2) 
end.

(* These examples help you see how zero_exp shall work *)
Example zero_exp_1:
  zero_exp (ALit 42) = ALit 42.
Proof.
reflexivity.
Qed.

Example zero_exp_2:
  zero_exp (AVar X) = ALit 0.
Proof.
reflexivity.
Qed.

Example zero_exp_3:
  zero_exp (APlus (ALit 42) (AVar Y)) = APlus (ALit 42) (ALit 0).
Proof.
reflexivity.
Qed.

(* Zeroed expressions are independent of the state *)
Lemma zero_stateindep:
  forall a:aexp, forall s:state,
  aeval a zero_state = aeval (zero_exp a) s.
Proof.
  intros. induction a. reflexivity.
  simpl in *. unfold zero_state. reflexivity.
  simpl. rewrite IHa1. rewrite IHa2. reflexivity.
Qed.

(* Zeroed expressions are closed *)
Lemma zero_closed:
  forall a:aexp,
  closed(zero_exp a).
Proof.
  intros. induction a. simpl. apply Cl_Lit. simpl. apply Cl_Lit.
  simpl. apply Cl_Plus. apply IHa1. apply IHa2.
Qed.

(* Zeroed expressions are bclosed *)
Lemma zero_bclosed:
  forall a:aexp,
  bclosed(zero_exp a) = true.
Proof.
  intros. induction a. reflexivity. unfold zero_exp. reflexivity.
  simpl. rewrite IHa1. rewrite IHa2. reflexivity.
Qed.