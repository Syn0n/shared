Inductive bool : Type :=
  | true
  | false.

Definition andb (b1:bool) (b2:bool) : bool :=
  match b1 with
  | true => b2
  | false => false
  end.

Definition orb (b1:bool) (b2:bool) : bool :=
  match b1 with
  | true => true
  | false => b2
  end.

Lemma l1:
  forall a b : bool, orb a (andb a b) = a.
Proof.
  intro a. destruct a. simpl. reflexivity.
  simpl. reflexivity.
Qed.


Lemma l2:
  forall a b : bool, andb a (orb a b) = a.
Proof.
  intro a. destruct a. simpl. reflexivity.
  simpl. reflexivity.
Qed.

(*  | O: nat         *)
(*  | S: nat -> nat. *)

Inductive nat: Type :=
  | O
  | S (n: nat).

Check O.
Check S.

Definition zero := O.
Definition one := S O.
Definition two := S (S O).
Definition three := S two.
Definition five := S (S three).

Theorem S_cong:
  forall n m: nat, n = m -> S n = S m.
Proof.
  intros.
  rewrite <- H.
  reflexivity.
Qed.

Theorem eqn_symn:
  forall n m: nat, n = m -> m = n.
Proof.
  (*intros. rewrite H. reflexivity.*)
  intros. symmetry. apply H.
Qed.

Fixpoint plusn (n m: nat) {struct n}: nat :=
  match n with
    | O => m
    | S n' => S ( plusn n' m)
  end.

Example plusn_test:
  plusn two three = five.
Proof.
  simpl. reflexivity.
Qed.

Fixpoint twicen (n: nat): nat :=
  match n with
    | O => O
    | S n' => S (S (twicen n'))    (*plusn n n*)
  end.

Example twicen_test:
  twicen two = S (S (S (S O))).
Proof.
  simpl.
  reflexivity.
Qed.

Notation "n + m" := (plusn n m)
  (at level 50, left associativity).

Theorem plusn_lid:
  forall n: nat, O + n = n.
Proof.
  intros.
  simpl.
reflexivity.
Qed.

Theorem plusn_rid:
  forall n: nat, n + O = n.
Proof.
  intros.
  destruct n as [|n'].
  simpl. reflexivity. simpl.
  destruct n' as [|n''].
  simpl. reflexivity. simpl.
Abort.

Theorem plusn_rid:
  forall n: nat, n + O = n.
Proof.
  intros.
  induction n as [|n'].
  simpl. reflexivity. simpl.
  rewrite IHn'.
  reflexivity.
Qed.

Theorem plusn_rid2:
  forall n:nat, n + O = n.
Proof.
  intros.
  induction n as [|n'].
  apply plusn_lid.
  simpl.
  apply S_cong.
  apply IHn'.
Qed.

Theorem plusn_rS:
  forall n m: nat, n + S m = S (n + m).
Proof.
  intros.
  induction n as [|n'].
  simpl. reflexivity.
  simpl.
  rewrite IHn'.
  reflexivity.
Qed.