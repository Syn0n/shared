From Coq Require Import Strings.String.
Definition ident : Type := string.

Inductive aexp : Type :=
| ALit  (n : nat)
| AVar  (x : ident)
| APlus (a1 a2 : aexp).

Definition state : Type := ident -> nat.

Fixpoint aeval (a: aexp)(s: state) : nat :=
match a with
| ALit n => n
| AVar x => s x
| APlus a1 a2 => aeval a1 s + aeval a2 s
end.

Definition X:ident := "X"%string.
Definition Y:ident := "Y"%string.
Definition Z:ident := "Z"%string.

Fixpoint bclosed (a:aexp) : bool :=
match a with
| ALit n => true
| APlus a1 a2 => bclosed a1 && bclosed a2
| _ => false
end.

From Coq Require Import Bool.

Lemma bclosed_stateindep:
 forall a:aexp, forall s s' :state,
 bclosed a = true -> aeval a s = aeval a s'.
Proof.
  intros. induction a.
  (* ALit *) reflexivity.
  (* AVar *) simpl in H. discriminate.
  (* APlus *) simpl in *.
  rewrite andb_true_iff in H.
  destruct H.
  rewrite (IHa1 H).
  rewrite (IHa2 H0).
  reflexivity.

  (*simpl.
  rewrite IHa1. rewrite IHa2. reflexivity.
  simpl in H.  rewrite andb_true_iff in H.
  destruct H. exact H0.
  simpl in H. rewrite andb_true_iff in H.
  destruct H. exact H. *)
Qed.

Inductive closed: aexp -> Prop :=
| Cl_Lit (n: nat):
  closed (ALit n)
| Cl_Plus (a1 a2:aexp):
  closed a1 -> closed a2 -> closed (APlus a1 a2)
.

Example cl1:
  closed (ALit 1).
Proof.
  apply Cl_Lit.
Qed.

Example cl2:
  closed (APlus (ALit 1) (ALit 2)).
Proof.
  apply Cl_Plus. apply Cl_Lit. apply Cl_Lit.
Qed.


Lemma closed_stateindep:
  forall a: aexp, forall s s': state,
  closed a -> aeval a s = aeval a s'.
Proof.
  intros. induction H. simpl. reflexivity.
  simpl. rewrite IHclosed1. rewrite IHclosed2. reflexivity.
Qed.

Lemma bclosed_iff_closed:
  forall a: aexp, bclosed a = true <-> closed a.
Proof.
  intros. split.
  (* bclosed -> closed *) induction a.

    (* ALit *) intros. apply Cl_Lit.
    (* AVar *) intros. simpl in H. discriminate.
    (* APlus *) intros. simpl in H. rewrite andb_true_iff in H.
      destruct H. apply Cl_Plus. exact (IHa1 H). exact (IHa2 H0).

  (* closed -> bclosed *) intros. induction H.
    simpl. reflexivity. simpl.
    rewrite IHclosed1. rewrite IHclosed2.
    simpl. reflexivity.
Qed.

Search "&&".

Example ex1: bclosed (ALit 1) = true.
Proof.
  simpl. reflexivity.
Qed.


Example ex2: bclosed (AVar X) = false.
Proof.
  simpl. reflexivity.
Qed.

Example ex3: bclosed (APlus (ALit 1) (AVar Y)) = false.
Proof.
  simpl. reflexivity.
Qed.