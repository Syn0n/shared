Inductive bool: Type := true | false.

Definition bor (b1 b2: bool) :=
  match b1 with
    | true => true
    | false => b2
  end.

Eval compute in bor true false.
Eval compute in bor false false.

Lemma left_id: forall (b: bool), bor false b = b.
Proof.
simpl. reflexivity.
Qed.

Lemma right_id: forall (b: bool), bor b false = b.
Proof.
intro b.
destruct b.
(* b = true *)
simpl. reflexivity.
(* b = false *)
simpl. reflexivity.
Qed.

Lemma bor_comm: forall (b1 b2: bool), bor b1 b2 = bor b2 b1.
Proof.
intro b1. destruct b1.

(* b1 = true *)
simpl.

intro b2. destruct b2.

(* b2 = true *)
simpl. reflexivity.

simpl. reflexivity.
simpl.

intro b2. destruct b2.
simpl. reflexivity.
simpl. reflexivity.
Qed.

(*  b-and comm & assoc  *)

Definition band (b1 b2: bool) :=
  match b1 with
    | false => false
    | true => b2
  end.

Lemma band_comm: forall (b1 b2: bool), band b1 b2 = band b2 b1.
Proof.
intro b1. destruct b1.
simpl.
intro b2. destruct b2.
simpl. reflexivity.
simpl. reflexivity.

intro b2. destruct b2.
simpl. reflexivity.
simpl. reflexivity.
Qed.

Lemma band_assoc: forall (b1 b2 b3: bool), band (band b1 b2) b3 = band b1 (band b2 b3).
Proof.
intro b1. destruct b1. simpl.
intro b2. destruct b2. simpl. reflexivity.
intro b3. destruct b3. simpl. reflexivity. simpl. reflexivity.

intro b2. destruct b2.
intro b3. destruct b3. simpl. reflexivity. simpl. reflexivity.
intro b3. destruct b3. simpl. reflexivity. simpl. reflexivity.
Qed.