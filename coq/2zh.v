(*orb && andb*)
Inductive bool : Type :=
  | true
  | false.

Definition andb (b1:bool) (b2:bool) : bool :=
  match b1 with
  | true => b2
  | false => false
  end.

Definition orb (b1:bool) (b2:bool) : bool :=
  match b1 with
  | true => true
  | false => b2
  end.

Lemma l1:
  forall a b : bool, orb a (andb a b) = a.
Proof.
  intro a. destruct a. simpl. reflexivity.
  simpl. reflexivity.
Qed.


Lemma l2:
  forall a b : bool, andb a (orb a b) = a.
Proof.
  intro a. destruct a. simpl. reflexivity.
  simpl. reflexivity.
Qed.