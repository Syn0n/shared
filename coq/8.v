From Coq Require Import Strings.String.
Definition ident : Type := string.
Inductive aexp : Type :=
| ALit (n : nat)
| AVar (x : ident)
| APlus (a1 a2 : aexp).

Definition X:ident := "X"%string.
Definition Y:ident := "Y"%string.
Definition Z:ident := "Z"%string.

Definition state : Type := ident -> nat.
Fixpoint aeval (a: aexp)(s: state) : nat :=
match a with
| ALit n => n
| AVar x => s x
| APlus a1 a2 => aeval a1 s + aeval a2 s
end.

Definition aState : state :=
  fun x =>
    match x with
    | "X"%string => 2
    | "Y"%string => 3
    | _ => 0 (* default value for all (global) variables *)
    end.

Definition update(s:state)(x:ident)(n:nat): state :=
  fun y =>
    if eqb x y then n else s y.
Definition empty : state := fun _ => 0.

Definition anotherState: state :=
  update aState X 11.

Reserved Notation "c -=> n" (at level 50).
Inductive eval_bigstep: aexp * state -> nat -> Prop :=
| eval_lit n s:
  (ALit n, s) -=> n
| eval_var x s:
  (AVar x, s) -=> s x
| eval_plus a1 a2 n m s:
  (a1, s) -=> n ->
  (a2, s) -=> m ->
  (APlus a1 a2, s) -=> (n + m)
where "c -=> n" := (eval_bigstep c n).

Example ex1: (APlus (AVar X) (ALit 5), anotherState) -=> 16.
Proof.
  apply (eval_plus _ _ 11 5).
  apply eval_var.
  apply eval_lit.
Qed.

Lemma bigstep_deterministic:
  forall a:aexp, forall s:state, forall n m: nat,
    (a, s) -=> n -> (a, s) -=> m -> n = m.
Proof.
  intros. generalize dependent m.
  induction H.
  intros. inversion H0. reflexivity.
  intros. inversion H0. reflexivity.
  intros. inversion H1. rewrite (IHeval_bigstep1 n0 H6).
  rewrite (IHeval_bigstep2 m1 H7). reflexivity.
Qed.

Theorem denotational_iff_bigstepoperational:
  forall a:aexp, forall s:state, forall n:nat,
    aeval a s = n <-> (a, s) -=> n.
Proof.
  split.
    generalize dependent n.
    induction a.
      intros. subst. apply eval_lit.
      intros. subst. apply eval_var.
      simpl. intros. subst. apply eval_plus.
      apply IHa1. reflexivity. apply IHa2. reflexivity.
    intros.
    generalize dependent n.
    induction a.
      intros. simpl. inversion H. reflexivity.
      intros. simpl. inversion H. reflexivity.
      intros. simpl. inversion H.
      rewrite (IHa1 n0 H4). rewrite (IHa2 m H5). reflexivity.
Qed.