﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WarPiggyes.Model;
using WarPiggyes.Persistence;
using System.Threading.Tasks;

namespace WarPiggyesTest
{
    [TestClass]
    public class WarPiggyesGameModelTest
    {
        private WarPiggyesFileMockDataAccess _dataAccess;
        private WarPiggyesGameModel _model;
        private WarPiggyesTable _table;

        [TestInitialize]
        public void Initialize()
        {
            _dataAccess = new WarPiggyesFileMockDataAccess();
            _model = new WarPiggyesGameModel(_dataAccess);

            _model.ShowHeart += new EventHandler<WarPiggyesEventArgs>(Model_ShowHeart);
            _model.ClearFieldsExceptPigs += new EventHandler<WarPiggyesEventArgs>(Model_ClearFieldsExceptPigs);
            _model.GameOver += new EventHandler<WarPiggyesEventArgs>(Model_GameOver);
            _model.Grass += new EventHandler<WarPiggyesEventArgs>(Model_Grass);
            _model.PositionChanged += new EventHandler<WarPiggyesEventArgs>(Model_PositionChanged);
            _model.DirectionChanged += new EventHandler<WarPiggyesEventArgs>(Model_DirectionChanged);
            _model.PigFire += new EventHandler<WarPiggyesEventArgs>(Model_PigFire);
            _model.ShowFire += new EventHandler<WarPiggyesEventArgs>(Model_ShowFire);
            _model.OnePunch += new EventHandler<WarPiggyesEventArgs>(Model_OnePunch);
            _model.GetOnePunched += new EventHandler<WarPiggyesEventArgs>(Model_GetOnePunched);
            _model.GetFired += new EventHandler<WarPiggyesEventArgs>(Model_GetFired);
        }

        [TestMethod]
        public void WarPiggiesGameModelNewGameSmallTest()
        {
            Assert.AreEqual(_model.TimeCounter, -1);
            _model.GameSize = GameSize.Small;
            _model.NewGame();

            Assert.AreEqual(_model.Table.Size, 4);

            Assert.AreEqual(_model.Table.getPigHearts(1), 3);
            Assert.AreEqual(_model.Table.getPigHearts(2), 3);

            Assert.AreEqual(_model.Table.getPigDirection(1), 0); // WarPig1 looks up           0 - up     2 - down
            Assert.AreEqual(_model.Table.getPigDirection(2), 2); // WarPig1 looks down         1 - right  3 - left

            Assert.AreEqual(_model.Table.getPigPosition(1).Item1, _model.Table.Size - 1);    // WarPig1 i coordinate
            Assert.AreEqual(_model.Table.getPigPosition(1).Item2, _model.Table.Size / 2);    // WarPig1 j coordinate

            Assert.AreEqual(_model.Table.getPigPosition(2).Item1, 0);                         // WarPig2 i coordinate
            Assert.AreEqual(_model.Table.getPigPosition(2).Item2, _model.Table.Size / 2 - 1); // WarPig2 j coordinate
        }

        [TestMethod]
        public void WarPiggiesGameModelNewGameMediumTest()
        {
            _model.GameSize = GameSize.Medium;
            _model.NewGame();
            Assert.AreEqual(_model.Table.Size, 6);
        }

        [TestMethod]
        public void WarPiggiesGameModelNewGameLargeTest()
        {
            _model.GameSize = GameSize.Large;
            _model.NewGame();
            Assert.AreEqual(_model.Table.Size, 8);
        }

        private void Model_ShowHeart(Object sender, WarPiggyesEventArgs e) { }
        private void Model_ClearFieldsExceptPigs(Object sender, WarPiggyesEventArgs e) { }
        private void Model_Grass(Object sender, WarPiggyesEventArgs e) { }
        private void Model_PositionChanged(Object sender, WarPiggyesEventArgs e) { }
        private void Model_DirectionChanged(Object sender, WarPiggyesEventArgs e) { }
        private void Model_PigFire(Object sender, WarPiggyesEventArgs e) { }
        private void Model_ShowFire(Object sender, WarPiggyesEventArgs e) { }
        private void Model_OnePunch(Object sender, WarPiggyesEventArgs e) { }
        private void Model_GetOnePunched(Object sender, WarPiggyesEventArgs e) { }
        private void Model_GetFired(Object sender, WarPiggyesEventArgs e) { }
        private void Model_GameOver(Object sender, WarPiggyesEventArgs e)
        {
            Assert.AreEqual(e.Value1, 1);
        }

        [TestMethod]
        public void WarPiggiesGameModelStepTestByTimer()
        {
            _model.NewGame();
            String[] commands = { "forwards", "forwards", "onepunch", "onepunch", "fireball",
                                  "leftwards", "forwards", "forwards", "fireball", "onepunch"};
            _model.Step(commands);

            //Table = [0..3]x[0..3]
            //WarPig1.Pos = [3,2]  WarPig1.Dir = 0  WarPig1.Hp = 3
            //WarPig2.Pos = [0,1]  WarPig2.Dir = 2  WarPig2.Hp = 3
            _model.timer_Elapsed();
            Assert.AreEqual(_model.Table.getPigPosition(1).Item1, 2); // forwards
            Assert.AreEqual(_model.Table.getPigPosition(1).Item2, 2);

            Assert.AreEqual(_model.Table.getPigPosition(2).Item1, 0); // leftwards
            Assert.AreEqual(_model.Table.getPigPosition(2).Item2, 2);

            _model.timer_Elapsed();
            Assert.AreEqual(_model.Table.getPigPosition(1).Item1, 2); // forwards
            Assert.AreEqual(_model.Table.getPigPosition(1).Item2, 2);

            Assert.AreEqual(_model.Table.getPigPosition(2).Item1, 0); // forwards
            Assert.AreEqual(_model.Table.getPigPosition(2).Item2, 2); // they cant go to the same place [1,2] have to stay

            _model.timer_Elapsed();
            Assert.AreEqual(_model.Table.getPigHearts(1), 3); // onepunch hits WarPig2
            Assert.AreEqual(_model.Table.getPigHearts(2), 2); // forwards
            Assert.AreEqual(_model.Table.getPigPosition(2).Item1, 1);
            Assert.AreEqual(_model.Table.getPigPosition(2).Item2, 2);

            _model.timer_Elapsed();
            Assert.AreEqual(_model.Table.getPigHearts(1), 2); // onepunch hits WarPig2
            Assert.AreEqual(_model.Table.getPigHearts(2), 1); // fireball hits WarPig2


            _model.timer_Elapsed();
            // after 5. both Hp--
            //WarPig1.Pos = [2,2]  WarPig1.Dir = 0  WarPig1.Hp = 1
            //WarPig2.Pos = [1,2]  WarPig2.Dir = 2  WarPig2.Hp = 0
            Assert.AreEqual(_model.Table.getPigHearts(1), 1);
            Assert.AreEqual(_model.Table.getPigHearts(2), 0);

            Assert.AreEqual(_model.Table.getPigPosition(1).Item1, 2);
            Assert.AreEqual(_model.Table.getPigPosition(1).Item2, 2);

            Assert.AreEqual(_model.Table.getPigPosition(2).Item1, 1);
            Assert.AreEqual(_model.Table.getPigPosition(2).Item2, 2);
        }

        [TestMethod]
        public async Task LoadGameAsyncTest()
        {
            _table = await _dataAccess.LoadAsync("asd");
            Assert.AreEqual(_table.Size, 4);
            Assert.AreEqual(_table.getPigHearts(1), 3);
            Assert.AreEqual(_table.getPigDirection(1), 1);
            Assert.AreEqual(_table.getPigPosition(1).Item1, 3);
            Assert.AreEqual(_table.getPigPosition(1).Item2, 2);
            Assert.AreEqual(_table.getPigHearts(2), 3);
            Assert.AreEqual(_table.getPigDirection(2), 2);
            Assert.AreEqual(_table.getPigPosition(2).Item1, 2);
            Assert.AreEqual(_table.getPigPosition(2).Item2, 3);
        }

        [TestMethod]
        public async Task SaveGameAsyncTest()
        {
            await _dataAccess.SaveAsync("asdf", _table);
        }
    }
}
