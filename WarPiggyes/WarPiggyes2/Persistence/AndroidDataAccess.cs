﻿using System;
using System.IO;
using System.Threading.Tasks;
using WarPiggyes.Droid.Persistence;
using WarPiggyes.Persistence;
using Xamarin.Forms;

[assembly: Dependency(typeof(AndroidDataAccess))]
namespace WarPiggyes.Droid.Persistence
{
    /// <summary>
    /// Tic-Tac-Toe adatelérés megvalósítása Android platformra.
    /// </summary>
    public class AndroidDataAccess : IWarPiggyesDataAccess
    {
        /// <summary>
        /// Fájl betöltése.
        /// </summary>
        /// <param name="path">Elérési útvonal.</param>
        /// <returns>A beolvasott mezőértékek.</returns>
        public async Task<WarPiggyesTable> LoadAsync(String path)
        {
            // a betöltés a személyen könyvtárból történik
            String filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), path);

            // a fájlműveletet taszk segítségével végezzük (aszinkron módon)
            String[] values = (await Task.Run(() => File.ReadAllText(filePath))).Split(' ');

            Int32 tableSize = Int32.Parse(values[0]);
            WarPiggyesTable table = new WarPiggyesTable(tableSize);
            table.Size = tableSize;
            table.Player1.i = Int32.Parse(values[1]);
            table.Player1.j = Int32.Parse(values[2]);
            table.Player1.direction = Int32.Parse(values[3]);
            table.Player1.hp = Int32.Parse(values[4]);
            table.Player2.i = Int32.Parse(values[5]);
            table.Player2.j = Int32.Parse(values[6]);
            table.Player2.direction = Int32.Parse(values[7]);
            table.Player2.hp = Int32.Parse(values[8]);
            return table;
        }

        /// <summary>
        /// Fájl mentése.
        /// </summary>
        /// <param name="path">Elérési útvonal.</param>
        /// <param name="table">A fájlba kiírandó játéktábla.</param>
        public async Task SaveAsync(String path, WarPiggyesTable table)
        {
            String text = table.Size + " " +
                        table.Player1.i + " " + table.Player1.j + " " + table.Player1.direction + " " + table.Player1.hp + " " +
                        table.Player2.i + " " + table.Player2.j + " " + table.Player2.direction + " " + table.Player2.hp;

            // fájl létrehozása
            String filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), path);

            // kiírás (aszinkron módon)
            await Task.Run(() => File.WriteAllText(filePath, text));
        }
    }
}