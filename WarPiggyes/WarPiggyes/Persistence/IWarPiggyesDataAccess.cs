﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarPiggyes.Persistence
{
    public interface IWarPiggyesDataAccess
    {
        Task<WarPiggyesTable> LoadAsync(String path);
        Task SaveAsync(String fileName, WarPiggyesTable path);
    }
}
