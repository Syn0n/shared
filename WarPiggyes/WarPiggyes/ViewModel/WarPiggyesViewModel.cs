﻿using System;
using System.Collections.ObjectModel;
using WarPiggyes.Model;

namespace WarPiggyes.ViewModel
{
    public class WarPiggyesViewModel : ViewModelBase
    {
        #region Fields
        private WarPiggyesGameModel _model;
        private String _piggy1HpPic;
        private String _piggy2HpPic;
        private String[] Direction = { "up", "right", "down", "left" };
        private ObservableCollection<WarPiggyesField> _fields;
        String[] _commands;
        #endregion

        #region Properties
        public DelegateCommand NewGameCommand { get; private set; }
        public DelegateCommand LoadGameCommand { get; private set; }
        public DelegateCommand SaveGameCommand { get; private set; }
        public DelegateCommand ExitCommand { get; private set; }
        public DelegateCommand Pig1Button_Click_Command { get; private set; }
        public DelegateCommand Pig2Button_Click_Command { get; private set; }
        public DelegateCommand StartButton_Click_Command { get; private set; }
        public DelegateCommand CommandButton_Click_Command { get; private set; }
        public String Command0_Command
        {
            get { return _commands[0]; }
            set { if (_commands[0] != value) {  _commands[0] = value; OnPropertyChanged(); } }
        }
        public String Command1_Command
        {
            get { return _commands[1]; }
            set { if (_commands[1] != value) { _commands[1] = value; OnPropertyChanged(); } }
        }
        public String Command2_Command
        {
            get { return _commands[2]; }
            set { if (_commands[2] != value) { _commands[2] = value; OnPropertyChanged(); } }
        }
        public String Command3_Command
        {
            get { return _commands[3]; }
            set { if (_commands[3] != value) { _commands[3] = value; OnPropertyChanged(); } }
        }
        public String Command4_Command
        {
            get { return _commands[4]; }
            set { if (_commands[4] != value) { _commands[4] = value; OnPropertyChanged(); } }
        }
        public String Command5_Command
        {
            get { return _commands[5]; }
            set { if (_commands[5] != value) { _commands[5] = value; OnPropertyChanged(); } }
        }
        public String Command6_Command
        {
            get { return _commands[6]; }
            set { if (_commands[6] != value) { _commands[6] = value; OnPropertyChanged(); } }
        }
        public String Command7_Command
        {
            get { return _commands[7]; }
            set { if (_commands[7] != value) { _commands[7] = value; OnPropertyChanged(); } }
        }
        public String Command8_Command
        {
            get { return _commands[8]; }
            set { if (_commands[8] != value) { _commands[8] = value; OnPropertyChanged(); } }
        }
        public String Command9_Command
        {
            get { return _commands[9]; }
            set { if (_commands[9] != value) { _commands[9] = value; OnPropertyChanged(); } }
        }
        public ObservableCollection<WarPiggyesField> Fields
        {
            get
            { return _fields; }
            set
            {
                if (_fields != value)
                {
                    _fields = value;
                    OnPropertyChanged();
                }
            }
        }
        public Int32 TableSize
        {
            get
            { return _model.Table.Size; }
            set
            {
                if (_model.Table.Size != value)
                {
                    _model.Table.Size = value;
                    OnPropertyChanged();
                }
            }
        }
        public String Piggy1HpPic
        {
            get { return _piggy1HpPic; }
            set
            {
                if (_piggy1HpPic != value)
                {
                    _piggy1HpPic = value;
                    OnPropertyChanged();
                }
            }
        }
        public String Piggy2HpPic
        {
            get { return _piggy2HpPic; }
            set
            {
                if (_piggy2HpPic != value)
                {
                    _piggy2HpPic = value;
                    OnPropertyChanged();
                }
            }
        }


        public Boolean IsGameSmall
        {
            get { return _model.GameSize == GameSize.Small; }
            set
            {
                if (_model.GameSize == GameSize.Small)
                    return;

                _model.GameSize = GameSize.Small;
                OnPropertyChanged("IsGameSmall");
                OnPropertyChanged("IsGameMedium");
                OnPropertyChanged("IsGameLarge");
            }
        }

        public Boolean IsGameMedium
        {
            get { return _model.GameSize == GameSize.Medium; }
            set
            {
                if (_model.GameSize == GameSize.Medium)
                    return;

                _model.GameSize = GameSize.Medium;
                OnPropertyChanged("IsGameSmall");
                OnPropertyChanged("IsGameMedium");
                OnPropertyChanged("IsGameLarge");
            }
        }

        public Boolean IsGameLarge
        {
            get { return _model.GameSize == GameSize.Large; }
            set
            {
                if (_model.GameSize == GameSize.Large)
                    return;

                _model.GameSize = GameSize.Large;
                OnPropertyChanged("IsGameSmall");
                OnPropertyChanged("IsGameMedium");
                OnPropertyChanged("IsGameLarge");
            }
        }
        #endregion

        #region Events
        public event EventHandler NewGame;
        public event EventHandler LoadGame;
        public event EventHandler SaveGame;
        public event EventHandler ExitGame;

        public event EventHandler pig1button_Click;
        public event EventHandler pig2button_Click;
        public event EventHandler startbutton_Click;
        public event EventHandler commandbutton_Click;
        #endregion

        #region Constructors
        public WarPiggyesViewModel(WarPiggyesGameModel model)
        {
            _model = model;
            _model.ClearFieldsExceptPigs += new EventHandler<WarPiggyesEventArgs>(Model_ClearFieldsExceptPigs);
            _model.Grass += new EventHandler<WarPiggyesEventArgs>(Model_Grass);
            _model.PositionChanged += new EventHandler<WarPiggyesEventArgs>(Model_PositionChanged);
            _model.DirectionChanged += new EventHandler<WarPiggyesEventArgs>(Model_DirectionChanged);
            _model.ShowHeart += new EventHandler<WarPiggyesEventArgs>(Model_ShowHeart);
            _model.PigFire += new EventHandler<WarPiggyesEventArgs>(Model_PigFire);
            _model.ShowFire += new EventHandler<WarPiggyesEventArgs>(Model_ShowFire);
            _model.OnePunch += new EventHandler<WarPiggyesEventArgs>(Model_OnePunch);
            _model.GetOnePunched += new EventHandler<WarPiggyesEventArgs>(Model_GetOnePunched);
            _model.GetFired += new EventHandler<WarPiggyesEventArgs>(Model_GetFired);
            _model.GenerateNewTable += new EventHandler<WarPiggyesEventArgs>(Model_GenerateNewTable);

            _commands = new String[10];

            NewGameCommand = new DelegateCommand(param => { OnNewGame(); RefreshTable(); RefreshHearts(); Model_GenerateNewTable(this, new WarPiggyesEventArgs(_model.Table.Size)); });
            LoadGameCommand = new DelegateCommand(param => { OnLoadGame(); RefreshTable(); RefreshHearts(); });
            SaveGameCommand = new DelegateCommand(param => OnSaveGame());

            ExitCommand = new DelegateCommand(param => OnExitGame());
            Pig1Button_Click_Command = new DelegateCommand(param => OnPig1Button_Click());
            Pig2Button_Click_Command = new DelegateCommand(param => OnPig2Button_Click());
            StartButton_Click_Command = new DelegateCommand(param => OnStartButton_Click());
            CommandButton_Click_Command = new DelegateCommand(param => OnCommandButton_Click());

            IsGameSmall = true;
            Generate_Table();
        }
        #endregion

        #region Private methods
        private void RefreshHearts()
        {
            Piggy1HpPic = "piggy1heart"
                    + _model.Table.getPigHearts(1) + ".png";
            Piggy2HpPic = "piggy2heart"
                    + _model.Table.getPigHearts(2) + ".png";
        }
        private void RefreshTable()
        {
            foreach (WarPiggyesField field in Fields)
            {
                if (_model.Table.getPigPosition(1).Item1 == field.I && _model.Table.getPigPosition(1).Item2 == field.J)
                    field.Picture = "piggy1" + Direction[_model.Table.getPigDirection(1)] + ".png";
                else if (_model.Table.getPigPosition(2).Item1 == field.I && _model.Table.getPigPosition(2).Item2 == field.J)
                    field.Picture = "piggy2" + Direction[_model.Table.getPigDirection(2)] + ".png";
                else { field.Picture = "grass.jpg"; }
            }
        }

        private void SetPic(Int32 index, String Image)
        {
            Fields[index].Picture = Image;
        }
        #endregion

        #region Game event handlers
        private void Model_ClearFieldsExceptPigs(object sender, WarPiggyesEventArgs e)
        {
            RefreshTable();
        }

        private void Model_Grass(object sender, WarPiggyesEventArgs e)
        {
            SetPic(e.Tuple1.Item1 * _model.Table.Size + e.Tuple1.Item2, "grass.jpg");
        }

        private void Model_PositionChanged(object sender, WarPiggyesEventArgs e)
        {
            SetPic(e.Tuple1.Item1 * _model.Table.Size + e.Tuple1.Item2, "piggy" + e.Value1 + Direction[e.Value2] + ".png");
        }

        private void Model_DirectionChanged(object sender, WarPiggyesEventArgs e)
        {
            SetPic(e.Tuple1.Item1 * _model.Table.Size + e.Tuple1.Item2, "piggy" + e.Value1 + Direction[e.Value2] + ".png");
        }

        private void Model_ShowHeart(object sender, WarPiggyesEventArgs e)
        {
            RefreshHearts();
        }

        private void Model_PigFire(object sender, WarPiggyesEventArgs e)
        {
            SetPic(e.Tuple1.Item1 * _model.Table.Size + e.Tuple1.Item2, "fire" + e.Value1 + Direction[e.Value2] + ".png");
        }

        private void Model_ShowFire(object sender, WarPiggyesEventArgs e)
        {
            SetPic(e.Tuple1.Item1 * _model.Table.Size + e.Tuple1.Item2, "fireball.jpg");
        }

        private void Model_OnePunch(object sender, WarPiggyesEventArgs e)
        {
            SetPic(e.Tuple1.Item1 * _model.Table.Size + e.Tuple1.Item2, "onepunch" + e.Value1 + Direction[e.Value2] + ".png");
        }

        private void Model_GetOnePunched(object sender, WarPiggyesEventArgs e)
        {
            SetPic(e.Tuple1.Item1 * _model.Table.Size + e.Tuple1.Item2, "piggy" + e.Value1 + Direction[e.Value2] + "punched.png");
        }

        private void Model_GetFired(object sender, WarPiggyesEventArgs e)
        {
            SetPic(e.Tuple1.Item1 * _model.Table.Size + e.Tuple1.Item2, "piggy" + e.Value1 + Direction[e.Value2] + "exploded.png");
        }

        private void Generate_Table()
        {
            Fields = new ObservableCollection<WarPiggyesField>();
            for (Int32 i = 0; i < TableSize; i++)
            {
                for (Int32 j = 0; j < TableSize; j++)
                {
                    if (j == TableSize / 2 && i == TableSize - 1)
                    {
                        Fields.Add(new WarPiggyesField
                        {
                            I = i,
                            J = j,
                            Size = TableSize,
                            Picture = "piggy1up.png",
                        });
                    }
                    else if (j == (_model.Table.Size / 2) - 1 && i == 0)
                    {
                        Fields.Add(new WarPiggyesField
                        {
                            I = i,
                            J = j,
                            Size = TableSize,
                            Picture = "piggy2down.png",
                        });
                    }
                    else
                    {
                        Fields.Add(new WarPiggyesField
                        {
                            I = i,
                            J = j,
                            Size = TableSize,
                            Picture = "grass.jpg",
                        });
                    }
                }
            }
            RefreshHearts();
        }

        private void Model_GenerateNewTable(object sender, WarPiggyesEventArgs e)
        {
            TableSize = e.Value1;
            OnPropertyChanged("TableSize");
            OnPropertyChanged("IsGameSmall");
            OnPropertyChanged("IsGameMedium");
            OnPropertyChanged("IsGameLarge");
            Generate_Table();
        }
        #endregion

        #region Event methods
        private void OnNewGame()
        {
            if (NewGame != null)
            {
                NewGame(this, EventArgs.Empty);
                OnPropertyChanged("TableSize");
            }
        }

        private void OnLoadGame()
        {
            if (LoadGame != null)
            {
                LoadGame(this, EventArgs.Empty);
                OnPropertyChanged("TableSize");
            }
        }

        private void OnSaveGame()
        {
            if (SaveGame != null)
                SaveGame(this, EventArgs.Empty);
        }

        private void OnExitGame()
        {
            if (ExitGame != null)
                ExitGame(this, EventArgs.Empty);
        }
        private void OnPig1Button_Click()
        {
            if (pig1button_Click != null)
                pig1button_Click(this, EventArgs.Empty);
        }

        private void OnPig2Button_Click()
        {
            if (pig2button_Click != null)
                pig2button_Click(this, EventArgs.Empty);
        }
        private void OnStartButton_Click()
        {
            if (startbutton_Click != null)
                startbutton_Click(this, EventArgs.Empty);
        }
        private void OnCommandButton_Click()
        {
            if (commandbutton_Click != null)
                commandbutton_Click(this, EventArgs.Empty);
        }

        #endregion
    }
}

