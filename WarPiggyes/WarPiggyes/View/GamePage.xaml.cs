﻿using DLToolkit.Forms.Controls;
using Xamarin.Forms;

namespace WarPiggyes.View
{
    public partial class GamePage : ContentPage
    {
        public GamePage()
        {
            InitializeComponent();
            FlowListView.Init();
        }
    }
}