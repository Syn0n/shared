﻿using System;
using WarPiggyes.Persistence;
using WarPiggyes.ViewModel;
using WarPiggyes.Model;
using WarPiggyes.View;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace WarPiggyes
{
    public class App : Application
    {
        #region Fields

        private String[] _commands;
        private Boolean _clicked1;
        private Boolean _clicked2;
        private Int32 _timeCounter;

        private IWarPiggyesDataAccess _warPiggyesDataAccess;
        private WarPiggyesGameModel _warPiggyesGameModel;
        private WarPiggyesViewModel _warPiggyesViewModel;
        private GamePage _gamePage;
        private SettingsPage _settingsPage;
        private CommandPage _commandPage;

        private IStore _store;
        private StoredGameBrowserModel _storedGameBrowserModel;
        private StoredGameBrowserViewModel _storedGameBrowserViewModel;
        private LoadGamePage _loadGamePage;
        private SaveGamePage _saveGamePage;

        private NavigationPage _mainPage;
        #endregion

        #region Application methods
        public App()
        {
            _clicked1 = false;
            _clicked2 = false;
            _commands = new String[10];

            // játék összeállítása
            _warPiggyesDataAccess = DependencyService.Get<IWarPiggyesDataAccess>(); // az interfész megvalósítását automatikusan megkeresi a rendszer

            _warPiggyesGameModel = new WarPiggyesGameModel(_warPiggyesDataAccess);
            _warPiggyesGameModel.GameOver += new EventHandler<WarPiggyesEventArgs>(WarPiggyesGameModel_GameOver);

            _warPiggyesViewModel = new WarPiggyesViewModel(_warPiggyesGameModel);
            _warPiggyesViewModel.NewGame += new EventHandler(WarPiggyesViewModel_NewGame);
            _warPiggyesViewModel.LoadGame += new EventHandler(WarPiggyesViewModel_LoadGame);
            _warPiggyesViewModel.SaveGame += new EventHandler(WarPiggyesViewModel_SaveGame);
            _warPiggyesViewModel.ExitGame += new EventHandler(WarPiggyesViewModel_ExitGame);
            _warPiggyesViewModel.commandbutton_Click += new EventHandler(WarPiggyesViewModel_CommandGame);
            _warPiggyesViewModel.pig1button_Click += new EventHandler(ViewModel_pig1button_Click);
            _warPiggyesViewModel.pig2button_Click += new EventHandler(ViewModel_pig2button_Click);
            _warPiggyesViewModel.startbutton_Click += new EventHandler(ViewModel_startbutton_Click);

            _gamePage = new GamePage();
            _gamePage.BindingContext = _warPiggyesViewModel;

            _settingsPage = new SettingsPage();
            _settingsPage.BindingContext = _warPiggyesViewModel;

            _commandPage = new CommandPage();
            _commandPage.BindingContext = _warPiggyesViewModel;

            // a játékmentések kezelésének összeállítása
            _store = DependencyService.Get<IStore>(); // a perzisztencia betöltése az adott platformon
            _storedGameBrowserModel = new StoredGameBrowserModel(_store);
            _storedGameBrowserViewModel = new StoredGameBrowserViewModel(_storedGameBrowserModel);
            _storedGameBrowserViewModel.GameLoading += new EventHandler<StoredGameEventArgs>(StoredGameBrowserViewModel_GameLoading);
            _storedGameBrowserViewModel.GameSaving += new EventHandler<StoredGameEventArgs>(StoredGameBrowserViewModel_GameSaving);

            _loadGamePage = new LoadGamePage();
            _loadGamePage.BindingContext = _storedGameBrowserViewModel;

            _saveGamePage = new SaveGamePage();
            _saveGamePage.BindingContext = _storedGameBrowserViewModel;

            // nézet beállítása
            _mainPage = new NavigationPage(_gamePage); // egy navigációs lapot használunk fel a három nézet kezelésére

            MainPage = _mainPage;
        }

        protected override void OnStart()
        {
            _warPiggyesGameModel.NewGame();
        }

        protected override void OnSleep()
        {
            // elmentjük a jelenleg folyó játékot
            try
            {
                Task.Run(async () => await _warPiggyesGameModel.SaveGame("SuspendedGame"));
            }
            catch { }
        }

        protected override void OnResume()
        {
            // betöltjük a felfüggesztett játékot, amennyiben van
            try
            {
                Task.Run(async () =>
                {
                    await _warPiggyesGameModel.LoadGame("SuspendedGame");
                });
            }
            catch { }

        }
        #endregion


        #region ViewModel event handlers
        /// <summary>
        /// Új játék indításának eseménykezelője.
        /// </summary>
        private void WarPiggyesViewModel_NewGame(object sender, EventArgs e)
        {
            _warPiggyesGameModel.NewGame();
        }

        /// <summary>
        /// Játék betöltésének eseménykezelője.
        /// </summary>
        private async void WarPiggyesViewModel_LoadGame(object sender, System.EventArgs e)
        {
            await _storedGameBrowserModel.UpdateAsync(); // frissítjük a tárolt játékok listáját
            await _mainPage.PushAsync(_loadGamePage); // átnavigálunk a lapra
        }

        /// <summary>
        /// Játék mentésének eseménykezelője.
        /// </summary>
        private async void WarPiggyesViewModel_SaveGame(object sender, EventArgs e)
        {
            await _storedGameBrowserModel.UpdateAsync(); // frissítjük a tárolt játékok listáját
            await _mainPage.PushAsync(_saveGamePage); // átnavigálunk a lapra
        }

        /// <summary>
        /// Játék mentésének eseménykezelője.
        /// </summary>
        private async void WarPiggyesViewModel_CommandGame(object sender, EventArgs e)
        {
            await _mainPage.PushAsync(_commandPage); // átnavigálunk a lapra
        }

        private void ViewModel_pig1button_Click(object sender, EventArgs e)
        {
            _commands[0] = _warPiggyesViewModel.Command0_Command;
            _commands[1] = _warPiggyesViewModel.Command1_Command;
            _commands[2] = _warPiggyesViewModel.Command2_Command;
            _commands[3] = _warPiggyesViewModel.Command3_Command;
            _commands[4] = _warPiggyesViewModel.Command4_Command;
            _clicked1 = true;
            _warPiggyesViewModel.Command0_Command = "";
            _warPiggyesViewModel.Command1_Command = "";
            _warPiggyesViewModel.Command2_Command = "";
            _warPiggyesViewModel.Command3_Command = "";
            _warPiggyesViewModel.Command4_Command = "";
        }

        private void ViewModel_pig2button_Click(object sender, EventArgs e)
        {
            _commands[5] = _warPiggyesViewModel.Command5_Command;
            _commands[6] = _warPiggyesViewModel.Command6_Command;
            _commands[7] = _warPiggyesViewModel.Command7_Command;
            _commands[8] = _warPiggyesViewModel.Command8_Command;
            _commands[9] = _warPiggyesViewModel.Command9_Command;
            _clicked2 = true;
            _warPiggyesViewModel.Command5_Command = "";
            _warPiggyesViewModel.Command6_Command = "";
            _warPiggyesViewModel.Command7_Command = "";
            _warPiggyesViewModel.Command8_Command = "";
            _warPiggyesViewModel.Command9_Command = "";
        }

        private void ViewModel_startbutton_Click(object sender, EventArgs e)
        {
            _timeCounter = 0;
            if (!_clicked1 || !_clicked2) return;
            _clicked1 = false;
            _clicked2 = false;
            _warPiggyesGameModel.Step(_commands);
            Device.StartTimer(TimeSpan.FromSeconds(1), () => { _warPiggyesGameModel.timer_Elapsed(); return ++_timeCounter < 5; });
        }

        private async void WarPiggyesViewModel_ExitGame(object sender, EventArgs e)
        {
            await _mainPage.PushAsync(_settingsPage); // átnavigálunk a beállítások lapra
        }


        /// <summary>
        /// Betöltés végrehajtásának eseménykezelője.
        /// </summary>
        private async void StoredGameBrowserViewModel_GameLoading(object sender, StoredGameEventArgs e)
        {
            await _mainPage.PopAsync(); // visszanavigálunk

            // betöltjük az elmentett játékot, amennyiben van
            try
            {
                await _warPiggyesGameModel.LoadGame(e.Name);
            }
            catch
            {
                await MainPage.DisplayAlert("WarPiggyes játék", "Sikertelen betöltés.", "OK");
            }
        }

        /// <summary>
        /// Mentés végrehajtásának eseménykezelője.
        /// </summary>
        private async void StoredGameBrowserViewModel_GameSaving(object sender, StoredGameEventArgs e)
        {
            await _mainPage.PopAsync(); // visszanavigálunk

            try
            {
                // elmentjük a játékot
                await _warPiggyesGameModel.SaveGame(e.Name);
            }
            catch { }

            await MainPage.DisplayAlert("WarPiggyes játék", "Sikeres mentés.", "OK");
        }

        #endregion

        #region Model event handlers

        /// <summary>
        /// Játék végének eseménykezelője.
        /// </summary>
        private async void WarPiggyesGameModel_GameOver(object sender, WarPiggyesEventArgs e)
        {

            if (e.Value1 == 0)
            {
                await MainPage.DisplayAlert("WarPiggyes játék", "DRAW! Gratulations WarPiggyes!!" + Environment.NewLine, "OK");
            }
            else
            {
                await MainPage.DisplayAlert("WarPiggyes játék", "Gratulations WarPiggy" + e.Value1 + ". U Won!", "OK");
            }
        }
        #endregion
    }
}
