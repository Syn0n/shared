﻿using System;
using System.Threading.Tasks;
using WarPiggyes.Persistence;

namespace WarPiggyes.Model
{
    public enum GameSize { Small, Medium, Large }
    public class WarPiggyesGameModel
    {
        #region Fields
        private IWarPiggyesDataAccess _dataAccess;
        private WarPiggyesTable _table;
        private Tuple<int, int>[] comDirVec = {
            Tuple.Create(-1, 0),
            Tuple.Create(0, 1),
            Tuple.Create(1, 0),
            Tuple.Create(0, -1)
        };
        private GameSize _gameSize;
        private Int32 _timeCounter;
        private String[] _commands;
        private Boolean _fired;
        private Boolean _punched;
        private Boolean _gameOver;

        #endregion

        #region Properties
        public GameSize GameSize { get { return _gameSize; } set { _gameSize = value; } }
        public WarPiggyesTable Table { get { return _table; } }
        public Int32 TimeCounter { get { return _timeCounter; } set { _timeCounter = value; } }
        public Boolean IsGameOver { get { return !_table.Player1.isAlive || !_table.Player2.isAlive; } }
        #endregion

        #region Events
        public event EventHandler<WarPiggyesEventArgs> ClearFieldsExceptPigs;
        public event EventHandler<WarPiggyesEventArgs> Grass;
        public event EventHandler<WarPiggyesEventArgs> PositionChanged;
        public event EventHandler<WarPiggyesEventArgs> DirectionChanged;
        public event EventHandler<WarPiggyesEventArgs> ShowHeart;
        public event EventHandler<WarPiggyesEventArgs> PigFire;
        public event EventHandler<WarPiggyesEventArgs> ShowFire;
        public event EventHandler<WarPiggyesEventArgs> OnePunch;
        public event EventHandler<WarPiggyesEventArgs> GetOnePunched;
        public event EventHandler<WarPiggyesEventArgs> GetFired;
        public event EventHandler<WarPiggyesEventArgs> GenerateNewTable;
        public event EventHandler<WarPiggyesEventArgs> GameOver;
        #endregion

        #region Constructor
        public WarPiggyesGameModel(IWarPiggyesDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
            _table = new WarPiggyesTable();
            _timeCounter = -1;
            _fired = false;
            _punched = false;
            _gameOver = false;
            _commands = new String[10];
        }
        #endregion

        #region Public game methods
        public void NewGame()
        {
            _commands = new String[10];
            _timeCounter = -1;
            _gameOver = false;
            switch (_gameSize)
            {
                case GameSize.Small: _table = new WarPiggyesTable(4); break;
                case GameSize.Medium: _table = new WarPiggyesTable(6); break;
                case GameSize.Large: _table = new WarPiggyesTable(8); break;
            }
        }

        public void timer_Elapsed()
        {
            if (_gameOver) return;
            ++_timeCounter;
            ClearFieldsExceptPigs(this, new WarPiggyesEventArgs(
                    _table.getPigDirection(1), _table.getPigPosition(1),
                    _table.getPigDirection(2), _table.getPigPosition(2)));
            if (bothNeedsThePlace(_commands[_timeCounter], _commands[_timeCounter + 5])) { return; }
            Boolean canMove1 = checkMove(1, _commands[_timeCounter]);
            Boolean canMove2 = checkMove(2, _commands[_timeCounter + 5]);

            if (canMove1) actsWarPig(1, _commands[_timeCounter]);
            if (canMove2) actsWarPig(2, _commands[_timeCounter + 5]);
            checkGameOver();
        }

        public void Step(String[] commands)
        {
            for (Int32 i = 0; i < 10; i++)
            {
                _commands[i] = commands[i];
            }
            _timeCounter = -1;
        }
        public async Task LoadGame(String path)
        {
            if (_dataAccess == null)
                throw new InvalidOperationException("No data access is provided.");
            _table = await _dataAccess.LoadAsync(path);
            switch (Table.Size)
            {
                case 4: GameSize = GameSize.Small; break;
                case 6: GameSize = GameSize.Medium; break;
                case 8: GameSize = GameSize.Large; break;
            }
            _timeCounter = -1;
            GenerateNewTable(this, new WarPiggyesEventArgs(_table.Size));
            ShowHeart(this, new WarPiggyesEventArgs(1, _table.getPigHearts(1)));
            ShowHeart(this, new WarPiggyesEventArgs(2, _table.getPigHearts(2)));
            ClearFieldsExceptPigs(this, new WarPiggyesEventArgs(
                    _table.getPigDirection(1), _table.getPigPosition(1),
                    _table.getPigDirection(2), _table.getPigPosition(2)));
        }

        public async Task SaveGame(String path)
        {
            if (_dataAccess == null)
                throw new InvalidOperationException("No data access is provided.");
            await _dataAccess.SaveAsync(path, _table);
        }

        #endregion

        #region Private game methods
        private Boolean checkMove(Int32 id, String command)
        {
            if (command == "forwards" || command == "rightwards" ||
                    command == "backwards" || command == "leftwards") return !noPositionChange(command, id);
            return true;
        }

        private Int32 getOpponentId(Int32 id)
        {
            return (id == 1) ? 2 : 1;
        }

        private Boolean outOfTable(Tuple<Int32, Int32> pos)
        {
            return pos.Item1 < 0 || pos.Item2 < 0 || pos.Item1 >= _table.Size || pos.Item2 >= _table.Size;
        }

        private Boolean noPositionChange(String command, Int32 id)
        {
            Tuple<Int32, Int32> MiraiPos = getMiraiPositionPig(_table.getPigPosition(id), _table.getPigDirection(id), command);
            return (command != "forwards" && command != "backwards" && command != "rightwards" && command != "leftwards") ||
                outOfTable(MiraiPos) || equalTuples(MiraiPos, _table.getPigPosition(getOpponentId(id)));
        }

        private Tuple<Int32, Int32> sumOfTuples(Tuple<Int32, Int32> first, Tuple<Int32, Int32> second)
        {
            return Tuple.Create(first.Item1 + second.Item1, first.Item2 + second.Item2);
        }

        private Boolean equalTuples(Tuple<Int32, Int32> first, Tuple<Int32, Int32> second)
        {
            return first.Item1 == second.Item1 && first.Item2 == second.Item2;
        }

        private Tuple<Int32, Int32> getMiraiPositionPig(Tuple<Int32, Int32> pigPosition, Int32 pigDir, String command)
        {
            if (command == "forwards")
                return Tuple.Create(pigPosition.Item1 + comDirVec[pigDir].Item1, pigPosition.Item2 + comDirVec[pigDir].Item2);
            if (command == "rightwards")
                return Tuple.Create(pigPosition.Item1 + comDirVec[(pigDir + 1) % 4].Item1, pigPosition.Item2 + comDirVec[(pigDir + 1) % 4].Item2);
            if (command == "backwards")
                return Tuple.Create(pigPosition.Item1 + comDirVec[(pigDir + 2) % 4].Item1, pigPosition.Item2 + comDirVec[(pigDir + 2) % 4].Item2);
            if (command == "leftwards")
                return Tuple.Create(pigPosition.Item1 + comDirVec[(pigDir + 3) % 4].Item1, pigPosition.Item2 + comDirVec[(pigDir + 3) % 4].Item2);
            return pigPosition;
        }

        private Int32 getMiraiDirectionPig(Int32 pigDir, String command)
        {
            if (command == "turn right")
                return (pigDir + 1) % 4;
            if (command == "turn left")
                return (pigDir - 1) % 4;
            return pigDir;
        }

        private Boolean bothNeedsThePlace(String command1, String command2)
        {
            Tuple<Int32, Int32> miraiPosition1 = (noPositionChange(command1, 1) ? _table.getPigPosition(1)
                : getMiraiPositionPig(_table.getPigPosition(1), _table.getPigDirection(1), command1));
            Tuple<Int32, Int32> miraiPosition2 = (noPositionChange(command2, 2) ? _table.getPigPosition(2)
                : getMiraiPositionPig(_table.getPigPosition(2), _table.getPigDirection(2), command2));
            return equalTuples(miraiPosition1, miraiPosition2);
        }

        private void actsWarPig(Int32 id, String command)
        {
            if (command == "forwards" || command == "rightwards" ||
                command == "backwards" || command == "leftwards") { move(id, command); return; }
            if (command == "turn right") { turn(id, 1); return; }
            if (command == "turn left") { turn(id, 3); return; }
            if (command == "fireball") { checkPositionForFire(id); return; }
            if (command == "onepunch") { checkPositionForOnePunch(id); return; }
        }

        private void move(Int32 id, String command)
        {
            Grass(this, new WarPiggyesEventArgs(_table.getPigPosition(id)));
            _table.setPigPosition(id, getMiraiPositionPig(_table.getPigPosition(id), _table.getPigDirection(id), command));
            if (!_fired && !_punched)
                PositionChanged(this, new WarPiggyesEventArgs(id, _table.getPigDirection(id), _table.getPigPosition(id)));
            _fired = false;
            _punched = false;
        }

        private void turn(Int32 id, Int32 changeDir)
        {
            _table.setPigDirection(id, (_table.getPigDirection(id) + changeDir) % 4);
            if (!_fired && !_punched)
                DirectionChanged(this, new WarPiggyesEventArgs(id, _table.getPigDirection(id), _table.getPigPosition(id)));
            _fired = false;
            _punched = false;
        }

        private void fire(Int32 i, Int32 j, Int32 direction, Int32 id)
        {
            Int32 opponent = getOpponentId(id);
            Tuple<Int32, Int32> opposition = (id == 2) ? _table.getPigPosition(opponent) :
                getMiraiPositionPig(_table.getPigPosition(opponent), _table.getPigDirection(opponent), _commands[_timeCounter + 5]);
            PigFire(this, new WarPiggyesEventArgs(id, direction, Tuple.Create(i, j)));
            i += comDirVec[direction].Item1;
            j += comDirVec[direction].Item2;
            while (!outOfTable(Tuple.Create(i, j)))
            {
                if (equalTuples(Tuple.Create(i, j), opposition))
                {
                    if (opponent == 2) _fired = true;
                    _table.damaged(opponent);
                    ShowHeart(this, new WarPiggyesEventArgs(opponent, _table.getPigHearts(opponent)));
                    if (opponent == 2 || (!_fired && !_punched))
                        GetFired(this, new WarPiggyesEventArgs(opponent, (id == 2) ? _table.getPigDirection(opponent) :
                        getMiraiDirectionPig(_table.getPigDirection(opponent), _commands[_timeCounter + 5]), opposition));
                    else
                    {
                        _fired = false;
                        _punched = false;
                    }
                }
                else
                {
                    ShowFire(this, new WarPiggyesEventArgs(Tuple.Create(i, j)));
                }
                i += comDirVec[direction].Item1;
                j += comDirVec[direction].Item2;
            }
        }

        private void checkPositionForFire(Int32 id)
        {
            Tuple<Int32, Int32> pos = _table.getPigPosition(id);
            switch (_table.getPigDirection(id))
            {
                case (0): fire(pos.Item1, pos.Item2, 0, id); break;
                case (1): fire(pos.Item1, pos.Item2, 1, id); break;
                case (2): fire(pos.Item1, pos.Item2, 2, id); break;
                case (3): fire(pos.Item1, pos.Item2, 3, id); break;
            }
        }

        private void checkPositionForOnePunch(Int32 id)
        {
            Tuple<Int32, Int32> pos = _table.getPigPosition(id);
            Int32 opponent = getOpponentId(id);
            OnePunch(this, new WarPiggyesEventArgs(id, _table.getPigDirection(id), pos));

            onePunch(opponent, pos);
            onePunch(opponent, sumOfTuples(pos, comDirVec[0]));
            onePunch(opponent, sumOfTuples(sumOfTuples(pos, comDirVec[0]), comDirVec[1]));
            onePunch(opponent, sumOfTuples(pos, comDirVec[1]));
            onePunch(opponent, sumOfTuples(sumOfTuples(pos, comDirVec[1]), comDirVec[2]));
            onePunch(opponent, sumOfTuples(pos, comDirVec[2]));
            onePunch(opponent, sumOfTuples(sumOfTuples(pos, comDirVec[2]), comDirVec[3]));
            onePunch(opponent, sumOfTuples(pos, comDirVec[3]));
            onePunch(opponent, sumOfTuples(sumOfTuples(pos, comDirVec[3]), comDirVec[0]));
        }

        private void onePunch(Int32 opponent, Tuple<Int32, Int32> toPunch)
        {
            if (outOfTable(toPunch)) return;
            Tuple<Int32, Int32> opposition = (opponent == 1) ? _table.getPigPosition(opponent) :
                getMiraiPositionPig(_table.getPigPosition(opponent), _table.getPigDirection(opponent), _commands[_timeCounter + 5]);
            if (equalTuples(toPunch, opposition))
            {
                if (opponent == 2) _punched = true;
                _table.damaged(opponent);
                ShowHeart(this, new WarPiggyesEventArgs(opponent, _table.getPigHearts(opponent)));
                if ((opponent == 2 || (!_fired && !_punched)))
                    if (!equalTuples(_table.getPigPosition(getOpponentId(opponent)), opposition))
                        GetOnePunched(this, new WarPiggyesEventArgs(opponent, (opponent == 1) ? _table.getPigDirection(opponent) :
                        getMiraiDirectionPig(_table.getPigDirection(opponent), _commands[_timeCounter + 5]), opposition));
                else
                {
                    _fired = false;
                    _punched = false;
                }
            }
        }

        private void checkGameOver()
        {
            if (!_table.isPigAlive(1) && !_table.isPigAlive(2))
            {
                GameOver(this, new WarPiggyesEventArgs(0));
                _gameOver = true;
            }
            else if (!_table.isPigAlive(1))
            {
                GameOver(this, new WarPiggyesEventArgs(getOpponentId(1)));
                _gameOver = true;
            }
            else if (!_table.isPigAlive(2))
            {
                GameOver(this, new WarPiggyesEventArgs(getOpponentId(2)));
                _gameOver = true;
            }
        }
        #endregion
    }
}