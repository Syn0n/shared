WarPigs Game:
    It is a multiplayer game. 2 WarPigs fight against each other until 1 of them die.

Table:
    4x4, 6x6, 8x8 chooseable tablesize   (Menu)
    [0..n-1]x[0..n-1] coordinates

------>  (tablesize == 4 on the map)
------------------------------------------------------------------------

    WP1Commands:          0     1     2     3        WP2Commands:
      command1         |-----|-----|-----|-----|      command1
      command2       0 |     | WP2 |     |     |      command2
      command3         |-----|-----|-----|-----|      command3
      command4       1 |     |     |     |     |      command4
      command5         |-----|-----|-----|-----|      command5
                     2 |     |     |     |     |      
    submitbutton1      |-----|-----|-----|-----|    submitbutton2
                     3 |     |     | WP1 |     |
      HP1 left         |-----|-----|-----|-----|      HP2 left

                          round start button
------------------------------------------------------------------------

    At the start both of the WarPigs stands on the right side of the midline.
    
    WarPig1[tablesize-1,tablesize/2] looks up
    WarPig2[0,tablesize/2-1] looks down

Commands:  (depends on where the WarPig is looking at ;)
    "forwards"      -- moving 1 field forward
    "rightwards"    -- moving 1 field to right
    "downwards"     -- moving 1 field backward
    "leftwards"     -- moving 1 field to right
    "turn right"    -- turning right
    "turn left"     -- turning left
    "onepunch"      -- punch on 3x3 around the WP

     |-----|-----|-----|       |-----|-----|-----|
     |  X  |  X  |  X  |       |  X  | WP2 |  X  |
     |-----|-----|-----|       |-----|-----|-----|
     |  X  | WP1 |  X  |       |  X  |  X  |  X  |
     |-----|-----|-----|       |-----|-----|-----|
     |  X  |  X  |  X  |       |     |     |     |
     |-----|-----|-----|       |-----|-----|-----|

    "fireball"      -- fire to the WarPigs looking direction
    
   -- WP1 is looking on the right    ------>
     |-----|-----|-----|-----|-----|-----|
     |     |     |     |     |     |     |
     |-----|-----|-----|-----|-----|-----|
     | WP1 |  X  |  X  |  X  |  X  |  X  |
     |-----|-----|-----|-----|-----|-----|
     |     |     |     |     |     |     |
     |-----|-----|-----|-----|-----|-----|

How to Play:

-Each round consists 2x five steps.
-Can't step on the same field at the same time.
-Can't step on the field where's the other WP. 
-The WarPigs move simultaneously.
-Give the commands submit and stay alive.
-Round starts after the middlebutton on
    the bottom of the table was clicked.
- U have 3 lives, use the commands wisely. 

Save/Load possible.
