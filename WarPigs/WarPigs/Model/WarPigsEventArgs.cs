﻿using System;

namespace WarPigs.Model
{
    public class WarPigsEventArgs : EventArgs {
        private Int32 _value1;
        private Int32 _value2;
        private String _command1;
        private String _command2;
        private Tuple<Int32, Int32> _tuple1;
        private Tuple<Int32, Int32> _tuple2;

        public Int32 Value1 { get { return _value1; } }
        public Int32 Value2 { get { return _value2; } }
        public String Command1 { get { return _command1; } }
        public String Command2 { get { return _command2; } }
        public Tuple<Int32, Int32> Tuple1 { get { return _tuple1; } }
        public Tuple<Int32, Int32> Tuple2 { get { return _tuple2; } }
        public WarPigsEventArgs(Int32 value1)
        {
            _value1 = value1;
        }
        public WarPigsEventArgs(Tuple<Int32, Int32> tuple1)
        {
            _tuple1 = tuple1;
        }
        public WarPigsEventArgs(Int32 value1, Int32 value2)
        {
            _value1 = value1;
            _value2 = value2;
        }
        public WarPigsEventArgs(String command1, String command2)
        {
            _command1 = command1;
            _command2 = command2;
        }

        public WarPigsEventArgs(Int32 value1, Tuple<Int32, Int32> tuple1)
        {
            _value1 = value1;
            _tuple1 = tuple1;
        }
        public WarPigsEventArgs(Int32 value1, Int32 value2, Tuple<Int32, Int32> tuple1)
        {
            _value1 = value1;
            _value2 = value2;
            _tuple1 = tuple1;
        }
        public WarPigsEventArgs(Int32 value1, Tuple<Int32, Int32> tuple1, Int32 value2, Tuple<Int32, Int32> tuple2) {
            _value1 = value1;
            _value2 = value2;
            _tuple1 = tuple1;
            _tuple2 = tuple2;
        }
    }
}