﻿using System;
using System.Threading.Tasks;

namespace WarPigs.Persistence
{
    public interface IWarPigsDataAccess
    {
        Task<WarPigsTable> LoadAsync(String path);
        Task SaveAsync(String fileName, WarPigsTable path);
    }
}
