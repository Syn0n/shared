﻿using WarPigs.Model;
using WarPigs.Persistence;
using System;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace WarPigs.View
{
    public partial class Form1 : Form
    {
        #region Fields
        private IWarPigsDataAccess _dataAccess;
        private WarPigsGameModel _model;
        private Button[,] _buttonGrid;
        private String[] _commands;
        private Boolean _clicked1, _clicked2;
        #endregion

        #region Constructors
        public Form1()
        {
            InitializeComponent();
        }
        #endregion

        #region Form event handlers
        private void Form1_Load(object sender, EventArgs e)
        {
            _dataAccess = new WarPigsFileDataAccess();

            _model = new WarPigsGameModel(_dataAccess);
            _model.ClearFieldsExceptPigs += new EventHandler<WarPigsEventArgs>(Game_ClearFieldsExceptPigs);
            _model.Grass += new EventHandler<WarPigsEventArgs>(Game_Grass);
            _model.PositionChanged += new EventHandler<WarPigsEventArgs>(Game_PositionChanged);
            _model.DirectionChanged += new EventHandler<WarPigsEventArgs>(Game_DirectionChanged);
            _model.ShowHeart += new EventHandler<WarPigsEventArgs>(Game_ShowHeart);
            _model.PigFire += new EventHandler<WarPigsEventArgs>(Game_PigFire);
            _model.ShowFire += new EventHandler<WarPigsEventArgs>(Game_ShowFire);
            _model.OnePunch += new EventHandler<WarPigsEventArgs>(Game_OnePunch);
            _model.GetOnePunched += new EventHandler<WarPigsEventArgs>(Game_GetOnePunched);
            _model.GetFired += new EventHandler<WarPigsEventArgs>(Game_GetFired);
            _model.GenerateNewTable += new EventHandler<WarPigsEventArgs>(Game_GenerateNewTable);
            _model.GameOver += new EventHandler<WarPigsEventArgs>(Game_GameOver);

            _commands = new String[10];

            _clicked1 = false;
            _clicked2 = false;

            GenerateTable();

            _model.NewGame();
        }
        #endregion

        #region Game event handlers

        private void Game_Grass(Object sender, WarPigsEventArgs e)
        {
             _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.grass;
        }

        private void Game_ClearFieldsExceptPigs(Object sender, WarPigsEventArgs e)
        {
            for (Int32 i = 0; i < _model.Table.Size; i++)
                for (Int32 j = 0; j < _model.Table.Size; j++)
                {
                    if (i == e.Tuple1.Item1 && j == e.Tuple1.Item2)
                        switch (e.Value1)
                        {
                            case 0: _buttonGrid[i, j].BackgroundImage = Properties.Resources.piggy1up; break;
                            case 1: _buttonGrid[i, j].BackgroundImage = Properties.Resources.piggy1right; break;
                            case 2: _buttonGrid[i, j].BackgroundImage = Properties.Resources.piggy1down; break;
                            case 3: _buttonGrid[i, j].BackgroundImage = Properties.Resources.piggy1left;  break;
                        }
                    else if (i == e.Tuple2.Item1 && j == e.Tuple2.Item2)
                        switch (e.Value2)
                        {
                            case 0: _buttonGrid[i, j].BackgroundImage = Properties.Resources.piggy2up; break;
                            case 1: _buttonGrid[i, j].BackgroundImage = Properties.Resources.piggy2right; break;
                            case 2: _buttonGrid[i, j].BackgroundImage = Properties.Resources.piggy2down; break;
                            case 3: _buttonGrid[i, j].BackgroundImage = Properties.Resources.piggy2left; break;
                        }
                    else
                    {
                        _buttonGrid[i, j].BackgroundImage = Properties.Resources.grass;
                    }
                }
        }

        private void Game_PositionChanged(Object sender, WarPigsEventArgs e)
        {
            if (e.Value1 == 1)
                switch (e.Value2)
                {
                    case 0: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy1up; break;
                    case 1: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy1right; break;
                    case 2: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy1down; break;
                    case 3: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy1left; break;
                }
            else if (e.Value1 == 2)
                switch (e.Value2)
                {
                    case 0: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy2up; break;
                    case 1: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy2right; break;
                    case 2: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy2down; break;
                    case 3: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy2left; break;
                }
        }

        private void Game_DirectionChanged(Object sender, WarPigsEventArgs e)
        {
            if (e.Value1 == 1)
                switch (e.Value2)
                {
                    case 0: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy1up; break;
                    case 1: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy1right; break;
                    case 2: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy1down; break;
                    case 3: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy1left; break;
                }
            else if (e.Value1 == 2)
                switch (e.Value2)
                {
                    case 0: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy2up; break;
                    case 1: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy2right; break;
                    case 2: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy2down; break;
                    case 3: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy2left; break;
                }
        }

        private void Game_ShowHeart(Object sender, WarPigsEventArgs e)
        {
            if (e.Value1 == 1)
                switch (e.Value2)
                {
                    case 0: pictureBox1.BackgroundImage = Properties.Resources.piggy1heart0; break;
                    case 1: pictureBox1.BackgroundImage = Properties.Resources.piggy1heart1; break;
                    case 2: pictureBox1.BackgroundImage = Properties.Resources.piggy1heart2; break;
                    case 3: pictureBox1.BackgroundImage = Properties.Resources.piggy1heart3; break;
                }
            else if (e.Value1 == 2)
                switch (e.Value2)
                {
                    case 0: pictureBox2.BackgroundImage = Properties.Resources.piggy2heart0; break;
                    case 1: pictureBox2.BackgroundImage = Properties.Resources.piggy2heart1; break;
                    case 2: pictureBox2.BackgroundImage = Properties.Resources.piggy2heart2; break;
                    case 3: pictureBox2.BackgroundImage = Properties.Resources.piggy2heart3; break;                  
                }
        }

        private void Game_PigFire(Object sender, WarPigsEventArgs e)
        {
            if (e.Value1 == 1)
                switch (e.Value2)
                {
                    case 0: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.fire1up; break;
                    case 1: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.fire1right; break;
                    case 2: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.fire1down; break;
                    case 3: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.fire1left; break;
                }
            else if (e.Value1 == 2)
                switch (e.Value2)
                {
                    case 0: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.fire2up; break;
                    case 1: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.fire2right; break;
                    case 2: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.fire2down; break;
                    case 3: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.fire2left; break;
                }
        }

        private void Game_ShowFire(Object sender, WarPigsEventArgs e)
        {
            _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.fireball;
        }

        private void Game_OnePunch(Object sender, WarPigsEventArgs e)
        {
            if (e.Value1 == 1)  
                switch (e.Value2)
                {
                    case 0: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.onepunch1up; break;
                    case 1: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.onepunch1right; break;
                    case 2: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.onepunch1down; break;
                    case 3: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.onepunch1left; break;
                }
            else if (e.Value1 == 2)
                switch (e.Value2)
                {
                    case 0: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.onepunch2up; break;
                    case 1: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.onepunch2right; break;
                    case 2: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.onepunch2down; break;
                    case 3: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.onepunch2left; break;
                }
        }

        private void Game_GetOnePunched(Object sender, WarPigsEventArgs e)
        {
            if (e.Value1 == 1)
                switch (e.Value2)
                {
                    case 0: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy1uppunched; break;
                    case 1: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy1rightpunched; break;
                    case 2: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy1downpunched; break;
                    case 3: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy1leftpunched; break;
                }
            else if (e.Value1 == 2)
                switch (e.Value2)
                {
                    case 0: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy2uppunched; break;
                    case 1: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy2rightpunched; break;
                    case 2: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy2downpunched; break;
                    case 3: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy2leftpunched; break;
                }
        }

        private void Game_GetFired(Object sender, WarPigsEventArgs e)
        {
            if (e.Value1 == 1)
                switch (e.Value2)
                {
                    case 0: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy1upexploded; break;
                    case 1: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy1rightexploded; break;
                    case 2: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy1downexploded; break;
                    case 3: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy1leftexploded; break;
                }
            else if (e.Value1 == 2)
                switch (e.Value2)
                {
                    case 0: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy2upexploded; break;
                    case 1: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy2rightexploded; break;
                    case 2: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy2downexploded; break;
                    case 3: _buttonGrid[e.Tuple1.Item1, e.Tuple1.Item2].BackgroundImage = Properties.Resources.piggy2leftexploded; break;
                }
        }

        private void Game_GenerateNewTable(Object sender, WarPigsEventArgs e)
        {
            DeleteButtons();
            GenerateTable();
        }

        private void Game_GameOver(Object sender, WarPigsEventArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                foreach (Button button in _buttonGrid)
                    button.Enabled = false;
                saveToolStripMenuItem.Enabled = false;
                button1.Enabled = false;
                button2.Enabled = false;
                button3.Enabled = false;

                if (e.Value1 == 0) MessageBox.Show("DRAW! Gratulations WarPigs!",
                                    "WarPigs",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Asterisk);
                else MessageBox.Show("Gratulations WarPig" + e.Value1 + ". U Won!",
                                    "WarPigs",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Asterisk);
            }));
        }

        private void goodPigButton_Click(object sender, EventArgs e)
        {
            _commands[0] = comboBox1.Text;
            _commands[1] = comboBox2.Text;
            _commands[2] = comboBox3.Text;
            _commands[3] = comboBox4.Text;
            _commands[4] = comboBox5.Text;
            _clicked1 = true;
            comboBox1.Text = String.Empty;
            comboBox2.Text = String.Empty;
            comboBox3.Text = String.Empty;
            comboBox4.Text = String.Empty;
            comboBox5.Text = String.Empty;
        }

        private void badPigButton_Click(object sender, EventArgs e)
        {
            _commands[5] = comboBox6.Text;
            _commands[6] = comboBox7.Text;
            _commands[7] = comboBox8.Text;
            _commands[8] = comboBox9.Text;
            _commands[9] = comboBox10.Text;
            _clicked2 = true;
            comboBox6.Text = String.Empty;
            comboBox7.Text = String.Empty;
            comboBox8.Text = String.Empty;
            comboBox9.Text = String.Empty;
            comboBox10.Text = String.Empty;
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            if (!_clicked1 || !_clicked2) return;
            _clicked1 = false;
            _clicked2 = false;
            _model.Step(_commands);
        }
        #endregion

        #region Menu event handlers
        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveToolStripMenuItem.Enabled = true;
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            DeleteButtons();
            _model.NewGame();
            GenerateTable();
        }

        private void small4x4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _model.GameSize = GameSize.Small;
        }

        private void medium6x6ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _model.GameSize = GameSize.Medium;
        }

        private void large8x8ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _model.GameSize = GameSize.Large;
        }

        private async void loadToolStripMenuItem_ClickAsync(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Int32 prevTabSize = _model.Table.Size;
                    await _model.LoadGameAsync(openFileDialog1.FileName);
                    saveToolStripMenuItem.Enabled = true;
                    button1.Enabled = true;
                    button2.Enabled = true;
                    button3.Enabled = true;
                }
                catch (WarPigsDataException)
                {
                    MessageBox.Show("Loading game wasn't successful!" + Environment.NewLine +
                        "Bad path, or bad file-format!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _model.NewGame();
                }
            }
        }

        private async void saveToolStripMenuItem_ClickAsync(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    await _model.SaveGameAsync(saveFileDialog1.FileName);
                }
                catch (WarPigsDataException)
                {
                    MessageBox.Show("Saving Game wasn't successful!" + Environment.NewLine +
                        "Bad path, or the dictionary doesn't have write permission.",
                        "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Hontoni do U want to escape?", "WarPigs Game",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Close();
            }
        }
        #endregion

        #region Private methods

        private void DeleteButtons()
        {
            for (Int32 i = 0; i < _buttonGrid.GetLength(0); i++)
                for (Int32 j = 0; j < _buttonGrid.GetLength(1); j++)
                {
                    if (Controls.Contains(_buttonGrid[i, j]))
                    {
                        Controls.Remove(_buttonGrid[i, j]);
                        _buttonGrid[i, j].Dispose();
                    }
                }
        }

        private void GenerateTable()
        {
            _buttonGrid = new Button[_model.Table.Size, _model.Table.Size];
            for (Int32 i = 0; i < _model.Table.Size; i++)
                for (Int32 j = 0; j < _model.Table.Size; j++)
                {
                    _buttonGrid[i, j] = new Button();
                    _buttonGrid[i, j].Location = new Point(260 + 600 / _model.Table.Size * j, 50 + 600 / _model.Table.Size * i);
                    _buttonGrid[i, j].Size = new Size(600 / _model.Table.Size, 600 / _model.Table.Size); 
                    _buttonGrid[i, j].Font = new Font(FontFamily.GenericSansSerif, 25, FontStyle.Bold); 
                    _buttonGrid[i, j].Enabled = true;
                    _buttonGrid[i, j].FlatAppearance.BorderSize = 0;
                    _buttonGrid[i, j].TabIndex = 100 + i * _model.Table.Size + j;
                    _buttonGrid[i, j].FlatStyle = FlatStyle.Flat;
                    if (i == 0 && j == _model.Table.Size / 2 - 1) _buttonGrid[i, j].BackgroundImage = Properties.Resources.piggy2down;
                    else if (i == _model.Table.Size - 1 && j == _model.Table.Size / 2) _buttonGrid[i, j].BackgroundImage = Properties.Resources.piggy1up;
                    else _buttonGrid[i, j].BackgroundImage = Properties.Resources.grass;
                    _buttonGrid[i, j].BackgroundImageLayout = ImageLayout.Stretch;

                    Controls.Add(_buttonGrid[i, j]);
                }
        }
        #endregion
    }
}
