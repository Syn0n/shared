﻿using System;
using System.Threading.Tasks;
using WarPiggies.Persistence;

namespace WarPiggiesTest
{
    class WarPiggiesFileMockDataAccess : IWarPiggiesDataAccess
    {
        public async Task<WarPiggiesTable> LoadAsync(String path)
        {
            String[] numbers = { "4", "3", "2", "1", "3", "2", "3", "2", "3" };
            Int32 tableSize = Int32.Parse(numbers[0]);
            WarPiggiesTable table = new WarPiggiesTable(tableSize);
            table.Size = tableSize;
            table.Player1.i = Int32.Parse(numbers[1]);
            table.Player1.j = Int32.Parse(numbers[2]);
            table.Player1.direction = Int32.Parse(numbers[3]);
            table.Player1.hp = Int32.Parse(numbers[4]);
            table.Player2.i = Int32.Parse(numbers[5]);
            table.Player2.j = Int32.Parse(numbers[6]);
            table.Player2.direction = Int32.Parse(numbers[7]);
            table.Player2.hp = Int32.Parse(numbers[8]);
            return table;
        }

        public async Task SaveAsync(String path, WarPiggiesTable table)
        {

        }
    }
}
