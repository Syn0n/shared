﻿using System;
using System.Threading.Tasks;

namespace WarPiggies.Persistence
{
    public interface IWarPiggiesDataAccess
    {
        Task<WarPiggiesTable> LoadAsync(String path);
        Task SaveAsync(String fileName, WarPiggiesTable path);
    }
}
