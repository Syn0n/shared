﻿using System;

namespace WarPiggies.Persistence
{
    public class WarPiggiesPig
    {
        Int32 _id;
        Int32 _hp;
        Int32 _i;
        Int32 _j;
        Int32 _direction;
        public WarPiggiesPig(Int32 tableSize, Int32 id) {
            _hp = 3;
            if (id == 1) { _j = tableSize/2; _i = tableSize - 1; _direction = 0; _id = id; }
            else if (id == 2) { _j = (tableSize/2)-1; _i = 0; _direction = 2; _id = id; }
        }
        public Int32 direction { get { return _direction; } set { _direction = value; } }
        public Int32 hp { get { return _hp; } set { _hp = value;  } }
        public Int32 i { get { return _i; } set { _i = value; } }
        public Int32 j { get { return _j; } set { _j = value; } }
        public bool isAlive { get { return _hp > 0; } }
        public void loseHeart() { hp--; }
    }
}
