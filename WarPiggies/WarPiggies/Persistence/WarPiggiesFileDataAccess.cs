﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace WarPiggies.Persistence
{
    public class WarPiggiesFileDataAccess : IWarPiggiesDataAccess
    {
        public async Task<WarPiggiesTable> LoadAsync(String path)
        {
            try
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    String line = await reader.ReadLineAsync();
                    String[] numbers = line.Split(' ');
                    Int32 tableSize = Int32.Parse(numbers[0]);
                    WarPiggiesTable table = new WarPiggiesTable(tableSize);
                    table.Size = tableSize;
                    table.Player1.i = Int32.Parse(numbers[1]);
                    table.Player1.j = Int32.Parse(numbers[2]);
                    table.Player1.direction = Int32.Parse(numbers[3]);
                    table.Player1.hp = Int32.Parse(numbers[4]);
                    table.Player2.i = Int32.Parse(numbers[5]);
                    table.Player2.j = Int32.Parse(numbers[6]);
                    table.Player2.direction = Int32.Parse(numbers[7]);
                    table.Player2.hp = Int32.Parse(numbers[8]);
                    return table;
                }
            }
            catch
            {
                throw new WarPiggiesDataException();
            }
        }

        public async Task SaveAsync(String path, WarPiggiesTable table)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(path))
                {
                    await writer.WriteLineAsync(table.Size + " " +
                        table.Player1.i + " " + table.Player1.j + " " + table.Player1.direction + " " + table.Player1.hp + " " +
                        table.Player2.i + " " + table.Player2.j + " " + table.Player2.direction + " " + table.Player2.hp);
                }
            }
            catch
            {
                throw new WarPiggiesDataException();
            }
        }
    }
}
