﻿using System;

namespace WarPiggies.Persistence
{
    public class WarPiggiesTable
    {
        #region Fields
        private WarPiggiesPig _player1;
        private WarPiggiesPig _player2;
        private Int32 _tableSize;
        #endregion

        #region Properties
        public Int32 Size { get { return _tableSize; } set { _tableSize = value; } }
        public WarPiggiesPig Player1 { get {  return _player1; } }
        public WarPiggiesPig Player2 { get { return _player2; } }
        #endregion

        #region Constructors
        public WarPiggiesTable() : this(4) { }

        public WarPiggiesTable(Int32 tableSize)
        {
            if (tableSize != 4 && tableSize != 6 && tableSize != 8)
                throw new ArgumentOutOfRangeException("The table size can be 4x4,6x6,8x8.", "tableSize");

            _tableSize = tableSize;
            _player1 = new WarPiggiesPig(tableSize, 1);
            _player2 = new WarPiggiesPig(tableSize, 2);
        }
        #endregion

        #region Public methods
        public Int32 getPigDirection(Int32 playerId)
        {
            switch (playerId)
            {
                case (1): return _player1.direction;
                case (2): return _player2.direction;
                default: return 0;
            }
        }

        public void setPigDirection(Int32 playerId, Int32 dir)
        {
            if (dir < 0 || dir > 4)
                throw new ArgumentOutOfRangeException("direction",
                    "The Direction coordinate is out of range at " + playerId + ". WarPig. <setPigDirection>");
            switch (playerId)
            {
                case (1): _player1.direction = dir; return;
                case (2): _player2.direction = dir; return;
            }
        }

        public Tuple<Int32, Int32> getPigPosition(Int32 playerId)
        {
            switch (playerId)
            {
                case (1): return Tuple.Create(_player1.i, _player1.j);
                case (2): return Tuple.Create(_player2.i, _player2.j);
                default: return Tuple.Create(0,0);
            }
        }

        public void setPigPosition(Int32 playerId, Tuple<Int32, Int32> pos)
        {
            if (pos.Item1 < 0 || pos.Item1 >= _tableSize)
                throw new ArgumentOutOfRangeException("i", "The I coordinate is out of range. <setPigPosition>");
            if (pos.Item2 < 0 || pos.Item2 >= _tableSize)
                throw new ArgumentOutOfRangeException("j", "The J coordinate is out of range. <setPigPosition>");
            switch (playerId)
            {
                case (1): _player1.i = pos.Item1; _player1.j = pos.Item2; return;
                case (2): _player2.i = pos.Item1; _player2.j = pos.Item2; return;
            }
        }

        public void damaged(Int32 playerId) {
            switch (playerId)
            {
                case (1): _player1.loseHeart(); break;
                case (2): _player2.loseHeart(); break;
            }
        }

        public Int32 getPigHearts(Int32 playerId)
        {
            switch (playerId)
            {
                case (1): return _player1.hp;
                case (2): return _player2.hp;
                default: return 0;
            }
        }

        public Boolean isPigAlive(Int32 playerId) {
            switch (playerId)
            {
                case (1): return _player1.isAlive;
                case (2): return _player2.isAlive;
                default: return false;
            }
        }
        #endregion
    }
}
