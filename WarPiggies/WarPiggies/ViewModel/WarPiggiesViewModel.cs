﻿using System;
using System.Collections.ObjectModel;
using WarPiggies.Model;

namespace WarPiggies.ViewModel
{
    public class WarPiggiesViewModel : ViewModelBase
    {
        #region Fields
        private WarPiggiesGameModel _model;
        private String _piggy1HpPic;
        private String _piggy2HpPic;
        private String[] Direction = {"up", "right", "down", "left"};
        private ObservableCollection<WarPiggiesField> _fields;
        #endregion

        #region Properties
        public DelegateCommand NewGameCommand { get; private set; }
        public DelegateCommand LoadGameCommand { get; private set; }
        public DelegateCommand SaveGameCommand { get; private set; }
        public DelegateCommand ExitCommand { get; private set; }
        public DelegateCommand Pig1Button_Click_Command { get; private set; }
        public DelegateCommand Pig2Button_Click_Command { get; private set; }
        public DelegateCommand StartButton_Click_Command { get; private set; }
        public ObservableCollection<WarPiggiesField> Fields
        {
            get
            { return _fields; }
            set
            {
                if (_fields != value)
                {
                    _fields = value;
                    OnPropertyChanged();
                }
            }
        }
        public Int32 TableSize
        {
            get
            { return _model.Table.Size; }
            set
            {
                if (_model.Table.Size != value)
                {
                    _model.Table.Size = value;
                    OnPropertyChanged();
                }
            }
        }
        public String Piggy1HpPic
        {
            get { return _piggy1HpPic; }
            set {
                if (_piggy1HpPic != value)
                {
                    _piggy1HpPic = value;
                    OnPropertyChanged();
                }
            }
        }
        public String Piggy2HpPic
        {
            get { return _piggy2HpPic; }
            set
            {
                if (_piggy2HpPic != value)
                {
                    _piggy2HpPic = value;
                    OnPropertyChanged();
                }
            }
        }


        public Boolean IsGameSmall
        {
            get { return _model.GameSize == GameSize.Small; }
            set
            {
                if (_model.GameSize == GameSize.Small)
                    return;

                _model.GameSize = GameSize.Small;
                OnPropertyChanged("IsGameSmall");
                OnPropertyChanged("IsGameMedium");
                OnPropertyChanged("IsGameLarge");
            }
        }

        public Boolean IsGameMedium
        {
            get { return _model.GameSize == GameSize.Medium; }
            set
            {
                if (_model.GameSize == GameSize.Medium)
                    return;

                _model.GameSize = GameSize.Medium;
                OnPropertyChanged("IsGameSmall");
                OnPropertyChanged("IsGameMedium");
                OnPropertyChanged("IsGameLarge");
            }
        }

        public Boolean IsGameLarge
        {
            get { return _model.GameSize == GameSize.Large; }
            set
            {
                if (_model.GameSize == GameSize.Large)
                    return;

                _model.GameSize = GameSize.Large;
                OnPropertyChanged("IsGameSmall");
                OnPropertyChanged("IsGameMedium");
                OnPropertyChanged("IsGameLarge");
            }
        }
        #endregion

        #region Events
        public event EventHandler NewGame;
        public event EventHandler LoadGame;
        public event EventHandler SaveGame;
        public event EventHandler ExitGame;

        public event EventHandler pig1button_Click;
        public event EventHandler pig2button_Click;
        public event EventHandler startbutton_Click;
        #endregion

        #region Constructors
        public WarPiggiesViewModel(WarPiggiesGameModel model)
        {
            _model = model;
            _model.ClearFieldsExceptPigs += new EventHandler<WarPiggiesEventArgs>(Model_ClearFieldsExceptPigs);
            _model.Grass += new EventHandler<WarPiggiesEventArgs>(Model_Grass);
            _model.PositionChanged += new EventHandler<WarPiggiesEventArgs>(Model_PositionChanged);
            _model.DirectionChanged += new EventHandler<WarPiggiesEventArgs>(Model_DirectionChanged);
            _model.ShowHeart += new EventHandler<WarPiggiesEventArgs>(Model_ShowHeart);
            _model.PigFire += new EventHandler<WarPiggiesEventArgs>(Model_PigFire);
            _model.ShowFire += new EventHandler<WarPiggiesEventArgs>(Model_ShowFire);
            _model.OnePunch += new EventHandler<WarPiggiesEventArgs>(Model_OnePunch);
            _model.GetOnePunched += new EventHandler<WarPiggiesEventArgs>(Model_GetOnePunched);
            _model.GetFired += new EventHandler<WarPiggiesEventArgs>(Model_GetFired);
            _model.GenerateNewTable += new EventHandler<WarPiggiesEventArgs>(Model_GenerateNewTable);

            NewGameCommand = new DelegateCommand(param => { OnNewGame(); RefreshTable(); RefreshHearts(); Model_GenerateNewTable(this, new WarPiggiesEventArgs(_model.Table.Size)); });
            LoadGameCommand = new DelegateCommand(param => { OnLoadGame(); RefreshTable(); RefreshHearts(); });
            SaveGameCommand = new DelegateCommand(param => OnSaveGame());
            ExitCommand = new DelegateCommand(param => OnExitGame());
            Pig1Button_Click_Command = new DelegateCommand(param => OnPig1Button_Click());
            Pig2Button_Click_Command = new DelegateCommand(param => OnPig2Button_Click());
            StartButton_Click_Command = new DelegateCommand(param => OnStartButton_Click());

            Generate_Table();
        }
        #endregion

        #region Private methods
        private void RefreshHearts()
        {
            Piggy1HpPic = "/WarPiggies;component/Rsources/piggy1heart"
                    + _model.Table.getPigHearts(1) + ".png";
            Piggy2HpPic = "/WarPiggies;component/Rsources/piggy2heart"
                    + _model.Table.getPigHearts(2) + ".png";
        }
        private void RefreshTable()
        {
            foreach (WarPiggiesField field in Fields)
            {
                if (_model.Table.getPigPosition(1).Item1 == field.I && _model.Table.getPigPosition(1).Item2 == field.J)
                    field.Picture = "/WarPiggies;component/Rsources/piggy1" + Direction[_model.Table.getPigDirection(1)] + ".png";
                else if (_model.Table.getPigPosition(2).Item1 == field.I && _model.Table.getPigPosition(2).Item2 == field.J)
                    field.Picture = "/WarPiggies;component/Rsources/piggy2" + Direction[_model.Table.getPigDirection(2)] + ".png";
                else { field.Picture = "/WarPiggies;component/Rsources/grass.jpg"; }
            }
        }

        private void SetPic(Int32 index, String Image)
        {
            Fields[index].Picture = Image;
        }
        #endregion

        #region Game event handlers
        private void Model_ClearFieldsExceptPigs(object sender, WarPiggiesEventArgs e)
        {
            RefreshTable();
        }

        private void Model_Grass(object sender, WarPiggiesEventArgs e)
        {
            SetPic(e.Tuple1.Item1 * _model.Table.Size + e.Tuple1.Item2, "/WarPiggies;component/Rsources/grass.jpg");
        }

        private void Model_PositionChanged(object sender, WarPiggiesEventArgs e)
        {
            SetPic(e.Tuple1.Item1 * _model.Table.Size + e.Tuple1.Item2, "/WarPiggies;component/Rsources/piggy"+ e.Value1 + Direction[e.Value2] + ".png");
        }

        private void Model_DirectionChanged(object sender, WarPiggiesEventArgs e)
        {
            SetPic(e.Tuple1.Item1 * _model.Table.Size + e.Tuple1.Item2, "/WarPiggies;component/Rsources/piggy" + e.Value1 + Direction[e.Value2] + ".png");
        }

        private void Model_ShowHeart(object sender, WarPiggiesEventArgs e)
        {
            RefreshHearts();
        }

        private void Model_PigFire(object sender, WarPiggiesEventArgs e)
        {
            SetPic(e.Tuple1.Item1 * _model.Table.Size + e.Tuple1.Item2, "/WarPiggies;component/Rsources/fire" + e.Value1 + Direction[e.Value2] + ".png");
        }

        private void Model_ShowFire(object sender, WarPiggiesEventArgs e)
        {
            SetPic(e.Tuple1.Item1 * _model.Table.Size + e.Tuple1.Item2, "/WarPiggies;component/Rsources/fireball.jpg");
        }

        private void Model_OnePunch(object sender, WarPiggiesEventArgs e)
        {
            SetPic(e.Tuple1.Item1 * _model.Table.Size + e.Tuple1.Item2, "/WarPiggies;component/Rsources/onepunch" + e.Value1 + Direction[e.Value2] + ".png");
        }

        private void Model_GetOnePunched(object sender, WarPiggiesEventArgs e)
        {
            SetPic(e.Tuple1.Item1 * _model.Table.Size + e.Tuple1.Item2, "/WarPiggies;component/Rsources/piggy" + e.Value1 + Direction[e.Value2] + "punched.png");
        }

        private void Model_GetFired(object sender, WarPiggiesEventArgs e)
        {
            SetPic(e.Tuple1.Item1 * _model.Table.Size + e.Tuple1.Item2, "/WarPiggies;component/Rsources/piggy" + e.Value1 + Direction[e.Value2] + "exploded.png");
        }

        private void Generate_Table()
        {
            Fields = new ObservableCollection<WarPiggiesField>();
            for (Int32 i = 0; i < TableSize; i++)
            {
                for (Int32 j = 0; j < TableSize; j++)
                {
                    if (j == TableSize / 2 && i == TableSize - 1)
                    {
                        Fields.Add(new WarPiggiesField
                        {
                            I = i,
                            J = j,
                            Picture = "/WarPiggies;component/Rsources/piggy1up.png",
                        });
                    }
                    else if (j == (_model.Table.Size / 2) - 1 && i == 0)
                    {
                        Fields.Add(new WarPiggiesField
                        {
                            I = i,
                            J = j,
                            Picture = "/WarPiggies;component/Rsources/piggy2down.png",
                        });
                    }
                    else
                    {
                        Fields.Add(new WarPiggiesField
                        {
                            I = i,
                            J = j,
                            Picture = "/WarPiggies;component/Rsources/grass.jpg",
                        });
                    }
                }
            }
            RefreshHearts();
        }

        private void Model_GenerateNewTable(object sender, WarPiggiesEventArgs e)
        {
            TableSize = e.Value1;
            OnPropertyChanged("TableSize");
            OnPropertyChanged("IsGameSmall");
            OnPropertyChanged("IsGameMedium");
            OnPropertyChanged("IsGameLarge");
            Generate_Table();
        }
        #endregion

        #region Event methods
        private void OnNewGame()
        {
            if (NewGame != null)
            {
                NewGame(this, EventArgs.Empty);
                OnPropertyChanged("TableSize");
            }              
        }

        private void OnLoadGame()
        {
            if (LoadGame != null)
            {
                LoadGame(this, EventArgs.Empty);
                OnPropertyChanged("TableSize");
            }               
        }

        private void OnSaveGame()
        {
            if (SaveGame != null)
                SaveGame(this, EventArgs.Empty);
        }

        private void OnExitGame()
        {
            if (ExitGame != null)
                ExitGame(this, EventArgs.Empty);
        }
        private void OnPig1Button_Click()
        {
            if (pig1button_Click != null)
                pig1button_Click(this, EventArgs.Empty);
        }

        private void OnPig2Button_Click()
        {
            if (pig2button_Click != null)
                pig2button_Click(this, EventArgs.Empty);
        }
        private void OnStartButton_Click()
        {
            if (startbutton_Click != null)
                startbutton_Click(this, EventArgs.Empty);
        }
        #endregion
    }
}

