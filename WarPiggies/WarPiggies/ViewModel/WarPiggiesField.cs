﻿using System;

namespace WarPiggies.ViewModel
{
    public class WarPiggiesField : ViewModelBase
    {
        private String _picture;
        public Int32 I { get; set; }

        public Int32 J { get; set; }

        public String Picture { get { return _picture; }
            set
            {
                if (_picture != value)
                {
                    _picture = value;
                    OnPropertyChanged();
                }              
            }
        }
    }
}
