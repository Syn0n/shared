﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Threading;
using WarPiggies.Model;
using WarPiggies.Persistence;
using WarPiggies.View;
using WarPiggies.ViewModel;
using Microsoft.Win32;

namespace WarPiggies
{
    public partial class App : Application
    {
        #region Fields
        private WarPiggiesGameModel _model;
        private WarPiggiesViewModel _viewModel;
        private MainWindow _view;
        private String[] _commands;
        private Boolean _clicked1;
        private Boolean _clicked2;
        #endregion

        #region Constructors
        public App()
        {
            Startup += new StartupEventHandler(App_Startup);
            _clicked1 = false;
            _clicked2 = false;
            _commands = new String[10];
        }
        #endregion

        #region Application event handlers
        private void App_Startup(object sender, StartupEventArgs e)
        {
            _model = new WarPiggiesGameModel(new WarPiggiesFileDataAccess());
            _model.GameOver += new EventHandler<WarPiggiesEventArgs>(Model_GameOver);
            _model.NewGame();

            // nézemodell létrehozása
            _viewModel = new WarPiggiesViewModel(_model);
            _viewModel.NewGame += new EventHandler(ViewModel_NewGame);
            _viewModel.ExitGame += new EventHandler(ViewModel_ExitGame);
            _viewModel.LoadGame += new EventHandler(ViewModel_LoadGame);
            _viewModel.SaveGame += new EventHandler(ViewModel_SaveGame);

            _viewModel.pig1button_Click += new EventHandler(ViewModel_pig1button_Click);
            _viewModel.pig2button_Click += new EventHandler(ViewModel_pig2button_Click);
            _viewModel.startbutton_Click += new EventHandler(ViewModel_startbutton_Click);

            // nézet létrehozása
            _view = new MainWindow();
            _view.DataContext = _viewModel;
            _view.Closing += new System.ComponentModel.CancelEventHandler(View_Closing); // eseménykezelés a bezáráshoz
            _view.Show();
        }
        #endregion

        #region View event handlers
        private void View_Closing(object sender, CancelEventArgs e)
        {
            if (MessageBox.Show("Are U sure, U want to escape?", "WarPiggies", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
            {
                e.Cancel = true;
            }
        }
        #endregion

        #region ViewModel event handlers
        private void ViewModel_NewGame(object sender, EventArgs e)
        {
            _model.NewGame();
        }

        private void ViewModel_pig1button_Click(object sender, EventArgs e)
        {
            _commands[0] = _view.CB1.Text;
            _commands[1] = _view.CB2.Text;
            _commands[2] = _view.CB3.Text;
            _commands[3] = _view.CB4.Text;
            _commands[4] = _view.CB5.Text;
            _clicked1 = true;
            _view.CB1.Text = String.Empty;
            _view.CB2.Text = String.Empty;
            _view.CB3.Text = String.Empty;
            _view.CB4.Text = String.Empty;
            _view.CB5.Text = String.Empty;
        }
        
        private void ViewModel_pig2button_Click(object sender, EventArgs e)
        {
            _commands[5] = _view.CB6.Text;
            _commands[6] = _view.CB7.Text;
            _commands[7] = _view.CB8.Text;
            _commands[8] = _view.CB9.Text;
            _commands[9] = _view.CB10.Text;
            _clicked2 = true;
            _view.CB6.Text = String.Empty;
            _view.CB7.Text = String.Empty;
            _view.CB8.Text = String.Empty;
            _view.CB9.Text = String.Empty;
            _view.CB10.Text = String.Empty;
        }
        
        private void ViewModel_startbutton_Click(object sender, EventArgs e)
        {
            if (!_clicked1 || !_clicked2) return;
            _clicked1 = false;
            _clicked2 = false;
            _model.Step(_commands);
        }

        private async void ViewModel_LoadGame(object sender, System.EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Title = "WarPiggies Loading..";
                openFileDialog.Filter = "WarPiggies|*.stl";
                if (openFileDialog.ShowDialog() == true)
                {
                    await _model.LoadGameAsync(openFileDialog.FileName);
                }
            }
            catch (WarPiggiesDataException)
            {
                MessageBox.Show("Loading wasn't successful!", "WarPiggies", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async void ViewModel_SaveGame(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Title = "WarPiggies Saving..";
                saveFileDialog.Filter = "WarPiggies tábla|*.stl";
                if (saveFileDialog.ShowDialog() == true)
                {
                    try
                    {
                        await _model.SaveGameAsync(saveFileDialog.FileName);
                    }
                    catch (WarPiggiesDataException)
                    {
                        MessageBox.Show("Save was successful!" + Environment.NewLine + "Bad path, or folder doesn't have permission!",
                            "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Saving wasn't successful!", "WarPiggies", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ViewModel_ExitGame(object sender, System.EventArgs e)
        {
            _view.Close();
        }
        #endregion

        #region Model event handlers
        private void Model_GameOver(object sender, WarPiggiesEventArgs e)
        {
            if (e.Value1 == 0) MessageBox.Show("DRAW! Gratulations WarPiggies!",
                                    "WarPigies",
                                    MessageBoxButton.OK,
                                    MessageBoxImage.Asterisk);
            else MessageBox.Show("Gratulations WarPiggy" + e.Value1 + ". U Won!",
                                    "WarPiggies",
                                    MessageBoxButton.OK,
                                    MessageBoxImage.Asterisk);
        }
        #endregion
    }
}